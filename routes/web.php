<?php
/*Admin Controls*/
Route::get('admin/', function () {
    return view('admin');
});
Route::get('admin_menu/', function () {
    return view('admin_menu');
});
Route::get('admin_tax/', function () {
    return view('admin_tax');
});
Route::get('admin_staff/', function () {
    return view('admin_staff');
});
Route::get('admin_kitchen/', function () {
    return view('admin_kitchen');
});
Route::get('admin_zone/', function () {
    return view('admin_zone');
});
Route::get('admin_table/', function () {
    return view('admin_table');
});
Route::get('admin_cuisine/', function () {
    return view('admin_cuisine');
});
Route::get('admin_tax_slab/', function () {
    return view('admin_tax_slab');
});
Route::get('admin_addon/', function () {
    return view('admin_addon');
});
Route::get('admin_category/', function () {
    return view('admin_category');
});
Route::get('admin_billtitle/', function () {
    return view('admin_billtitle');
});
Route::get('receipts/', function () {
    return view('payment');
});
Route::get('admin_supplier/', function () {
    return view('admin_supplier');
});
Route::get('admin_item_management/', function () {
    return view('admin_item_management');
});
Route::get('admin_incoming/', function () {
    return view('admin_incoming');
});
Route::get('ingredient/', function () {
    return view('ingredient');
});
Route::get('ingredient_category/', function () {
    return view('ingredient_category');
});
Route::get('ingredient_unit/', function () {
    return view('ingredient_unit');
});
Route::get('admin_recipe/', function () {
    return view('admin_recipe');
});
Route::get('add_recipe/', function () {
    return view('add_recipe');
});
Route::get('add_outgoing/', function () {
    return view('add_outgoing');
});
Route::get('view_incomings/', function () {
    return view('view_incomings');
});
Route::get('view_outgoing/', function () {
    return view('view_outgoing');
});
/*Route::get('kot/', function () {
    return view('kot');
});*/
/*Admin Controls end*/

/*Branches Details*/
Route::resource('Branch','BranchController');
Route::resource('Kitchen','KitchenController');
Route::resource('Staff','StaffController');
Route::resource('Zone','ZoneController');
Route::resource('Table','TableController');
Route::resource('Menu','MenuController');
Route::resource('Tax','TaxController');
Route::resource('TaxSlab','TaxSlabController');
Route::resource('Addon','AddonController');
Route::resource('Category','CategoryController');
Route::resource('Bill','BillController');
Route::resource('BillTitle','BillTitleController');
Route::resource('Dashboard','DashboardController');
Route::resource('Supplier','SupplierController');
Route::resource('Imanagement','ImanagementController');
Route::resource('Incoming','IncomingController');
Route::resource('Outgoing','OutgoingController');
Route::resource('Recipe','RecipeController');
Route::resource('Ingredient','IngredientController');
Route::resource('ingredient_category','IngredientCategoryController');
Route::resource('ingredient_unit','IngredientUnitController');
Route::resource('Stock','StockController');

Route::get('admin_branches','BranchController@AllBranch');
Route::get('admin_kitchen','KitchenController@AllKitchen');
Route::get('admin_staff','StaffController@AllStaff');
Route::get('admin_zone','ZoneController@AllZone');
Route::get('admin_table','TableController@AllTable');
Route::get('admin_menu','MenuController@AllMenu');
Route::get('admin_tax','TaxController@AllTax');
Route::get('admin_cuisine','CuisineController@AllCuisine');
Route::get('admin_tax_slab','TaxSlabController@AllTaxSlab');
Route::get('admin_addon','AddonController@AllAddons');
Route::get('admin_category','CategoryController@AllCategory');
Route::get('billing','BillController@AllBill');
Route::get('admin_billtitle','BillTitleController@AllBillTitle');
Route::get('admin','DashboardController@AllDashboard');
Route::get('admin_supplier','SupplierController@AllSupplier');
Route::get('admin_item_management','ImanagementController@AllImanagement');
Route::get('admin_incoming','IncomingController@AllIncome');
Route::get('admin_outgoing','OutgoingController@AllOutgoing');
Route::get('admin_recipe','RecipeController@AllRecipe');
Route::get('ingredient','IngredientController@AllIngredient');
Route::get('ingredient_category','IngredientCategoryController@AllIngredientCategory');
Route::get('ingredient_unit','IngredientUnitController@AllIngredientUnit');
Route::get('stocks','StockController@index');
Route::get('branch_incomings','OutgoingController@BranchIncomings');
Route::get('kot','KitchenController@Kot');


Route::get('/view_incoming/{id}','IncomingController@ViewIncoming');
Route::get('/add_incomelist/{id}','IncomingController@AddIncomingList');
Route::get('/add_outgoinglist/{id}','OutgoingController@AddOutgoingList');
Route::get('/add_recipelist/{id}','RecipeController@AddRecipeList');
Route::post('create_incominglist','IncomingController@incominglist');
Route::post('create_outgoinglist','OutgoingController@outgoinglist');
Route::post('create_recipelist','RecipeController@RecipeList');
Route::get('/view_outgoing/{id}','OutgoingController@ViewOutgoing');
Route::get('/view_recipe/{id}','RecipeController@ViewRecipe');
Route::get('stock/branch/stock/{id}','StockController@StockDetail');


Route::post('create_branch','BranchController@store');
Route::post('create_kitchen','KitchenController@store');
Route::post('create_staff','StaffController@store');
Route::post('create_zone','ZoneController@store');
Route::post('create_table','TableController@store');
Route::post('create_menu','MenuController@store');
Route::post('create_tax','TaxController@store');
Route::post('create_cuisine','CuisineController@store');
Route::post('create_tax_slab','TaxSlabController@store');
Route::post('create_addon','AddonController@store');
Route::post('create_category','CategoryController@store');
Route::post('create_supplier','SupplierController@store');
Route::post('create_imanagement','ImanagementController@store');
Route::post('create_outgoing','OutgoingController@store');
Route::post('create_recipe','RecipeController@store');
Route::post('create_incoming','IncomingController@store');
Route::post('create_outgoing','OutgoingController@store');
Route::post('create_ingredient','IngredientController@store');
Route::post('create_ingredient_category','IngredientCategoryController@store');
Route::post('create_ingredient_unit','IngredientUnitController@store');


Route::delete('/branch/delete/{id}', 'BranchController@destroy');
Route::delete('/kitchen/delete/{id}', 'KitchenController@destroy');
Route::delete('/staff/delete/{id}', 'StaffController@destroy');
Route::delete('/zone/delete/{id}', 'ZoneController@destroy');
Route::delete('/table/delete/{id}', 'TableController@destroy');
Route::delete('/menu/delete/{id}', 'MenuController@destroy');
Route::delete('/tax/delete/{id}', 'TaxController@destroy');
Route::delete('/cuisine/delete/{id}', 'CuisineController@destroy');
Route::delete('/tax_slab/delete/{id}', 'TaxSlabController@destroy');
Route::delete('/addon/delete/{id}', 'AddonController@destroy');
Route::delete('/category/delete/{id}', 'CategoryController@destroy');
Route::delete('/supplier/delete/{id}', 'SupplierController@destroy');
Route::delete('/itemmanagement/delete/{id}', 'ImanagementController@destroy');
Route::delete('/outgoing/delete/{id}', 'OutgoingController@destroy');
Route::delete('/recipe/delete/{id}', 'RecipeController@destroy');
Route::delete('/ingredient/delete/{id}', 'IngredientController@destroy');
Route::delete('/ingredient_category/delete/{id}', 'IngredientCategoryController@destroy');
Route::delete('/ingredient_unit/delete/{id}', 'IngredientUnitController@destroy');
Route::delete('/income/delete/{id}', 'IncomingController@destroy');
Route::delete('/branch_incoming/delete/{id}', 'OutgoingController@destroyIncoming');
Route::post('/branch_incoming/approval/{id}', 'OutgoingController@ApprovalIncoming');
Route::delete('/kot/reject/{id}', 'KitchenController@KotRejected');
Route::post('/kot/accept/{id}', 'KitchenController@KotAccept');
Route::post('/kot/close/{id}', 'KitchenController@KotClose');

Route::post('branch/edit', 'BranchController@update');
Route::post('staff/edit', 'StaffController@update');
Route::post('kitchen/edit', 'KitchenController@update');
Route::post('zone/edit', 'ZoneController@update');
Route::post('table/edit', 'TableController@update');
Route::post('menu/edit', 'MenuController@update');
Route::post('tax/edit', 'TaxController@update');
Route::post('cuisine/edit', 'CuisineController@update');
Route::post('tax_slab/edit', 'TaxSlabController@update');
Route::post('addon/edit', 'AddonController@update');
Route::post('category/edit', 'CategoryController@update');
Route::post('btitle/edit', 'BillTitleController@update');
Route::post('supplier/edit', 'SupplierController@update');
Route::post('itemmanagement/edit', 'ImanagementController@update');
Route::post('incoming/edit', 'IncomingController@update');
Route::post('outgoing/edit', 'OutgoingController@update');
Route::post('recipe/edit', 'RecipeController@update');
Route::post('ingredient/edit', 'IngredientController@update');
Route::post('ingredient_category/edit', 'IngredientCategoryController@update');
Route::post('ingredient_unit/edit', 'IngredientUnitController@update');
/*// End Branches Details*/

/*controls*/
Route::get('add_income','IncomingController@AddIncome');
Route::get('add_recipe','RecipeController@AddRecipe');
Route::get('add_outgoing','OutgoingController@AddOutgoing');
Route::get('api/get-master', 'BillController@index');
Route::get('api/orders/pending', 'BillController@pendingOrders');
Route::post('api/kitchen-orders/print', 'BillController@printOrder');
Route::post('api/kitchen-orders/status', 'BillController@checkKotStatus');
Route::post('api/orders/{bill}/payment', 'BillController@updatePayment');

Route::get('api/get-ingredient-list','IncomingController@getIngredientList');
Route::get('api/get-unit-list','IncomingController@getUnitList');
Route::get('api/get-category-list','MenuController@getCategoryList');
Route::get('api/get-state-list','CountryStateCityController@getStateList');
Route::get('api/get-city-list','CountryStateCityController@getCityList');
Route::get('api/get-chef-list','KitchenController@getChefList');
Route::get('api/get-table-list','BillController@getTableList');
Route::get('api/get-item-list','BillController@getItemList');
Route::get('api/get-recipe_item-list','RecipeController@getItemList');
Route::get('api/get-item-selected','BillController@getItemSelected')->name('get.menu-addons');
Route::get('api/generate-pdf','BillController@generatePDF');
Route::middleware(['auth'])->group(function(){
    Route::post('api/place-order', 'BillController@placeOrder');
});

Route::get('api/get-zone-list','TableController@getZoneList');
/*// end*/

Auth::routes(['register' => false]);
Route::get('/logout', 'Auth\LoginController@logout');
Route::middleware(['auth'])->group(function(){
    Route::get('/', function () {
        return view('index');
    });
});
Route::get('test-print', 'BillController@print');