<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientUnit extends Model
{
   	protected $fillable = [
        'id', 'unit', 'status'
     ];

     public function incoming()
    {
        return $this->hasMany('\App\Incoming');
    }

    public function outgoing()
    {
        return $this->hasMany('\App\Outgoing');
    }

    public function recipe()
    {
        return $this->hasMany('\App\Recipe');
    }
}
