<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillItem extends Model
{
    protected $fillable = [
        'itemable_id', 'itemable_type', 'bill_id', 'kitchen_id', 'quantity','price', 'total_amount', 'name', 'sgst', 'cgst'
    ];
    public function menus()
    {
        return $this->morphedByMany('App\Menu', 'itemable');
    }
    public function addons()
    {
        return $this->morphedByMany('App\Addon', 'itemable');
    }
    public function kitchen()
    {
        return $this->belongsTo('App\Kitchen');
    }
}
