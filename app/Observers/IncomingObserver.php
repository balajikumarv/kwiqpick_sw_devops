<?php

namespace App\Observers;

use App\Incoming;
use App\Stock;

class IncomingObserver
{
    /**
     * Handle the incoming "created" event.
     *
     * @param  \App\Incoming  $incoming
     * @return void
     */
    public function created(Incoming $incoming)
    {
        $stock = Stock::firstOrCreate(['branch_id' => null, 'ingredient_id' => $incoming->ingredient_id]);
        $stock->increment('quantity', $incoming->quantity);
    }

    /**
     * Handle the incoming "updated" event.
     *
     * @param  \App\Incoming  $incoming
     * @return void
     */
    public function updated(Incoming $incoming)
    {
        //
    }

    /**
     * Handle the incoming "deleted" event.
     *
     * @param  \App\Incoming  $incoming
     * @return void
     */
    public function deleted(Incoming $incoming)
    {
        Stock::where(['branch_id' => null, 'ingredient_id' => $incoming->ingredient_id])->increment('quantity', $incoming->quantity);
    }

    /**
     * Handle the incoming "restored" event.
     *
     * @param  \App\Incoming  $incoming
     * @return void
     */
    public function restored(Incoming $incoming)
    {
        //
    }

    /**
     * Handle the incoming "force deleted" event.
     *
     * @param  \App\Incoming  $incoming
     * @return void
     */
    public function forceDeleted(Incoming $incoming)
    {
        //
    }
}
