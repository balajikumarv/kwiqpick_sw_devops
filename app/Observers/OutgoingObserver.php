<?php

namespace App\Observers;

use App\Outgoing;
use App\Stock;

class OutgoingObserver
{
    /**
     * Handle the outgoing "created" event.
     *
     * @param  \App\Outgoing  $outgoing
     * @return void
     */
    public function created(Outgoing $outgoing)
    {
        Stock::where(['branch_id' => null, 'ingredient_id' => $outgoing->ingredient_id])
            ->decrement('quantity', $outgoing->quantity);
    }

    /**
     * Handle the outgoing "updated" event.
     *
     * @param  \App\Outgoing  $outgoing
     * @return void
     */
    public function updated(Outgoing $outgoing)
    {
        if ($outgoing->status == 1) {
            $stock = Stock::firstOrCreate(['branch_id' => $outgoing->branch_id, 'ingredient_id' => $outgoing->ingredient_id]);
            $stock->increment('quantity', $outgoing->quantity);
        }
    }

    /**
     * Handle the outgoing "deleted" event.
     *
     * @param  \App\Outgoing  $outgoing
     * @return void
     */
    public function deleted(Outgoing $outgoing)
    {
        Stock::where(['branch_id' => null, 'ingredient_id' => $outgoing->ingredient_id])
            ->increment('quantity', $outgoing->quantity);

        if ($outgoing->status == 1) {
            Stock::where(['branch_id' => $outgoing->branch_id, 'ingredient_id' => $outgoing->ingredient_id])
                ->decrement('quantity', $outgoing->quantity);
        }
    }

    /**
     * Handle the outgoing "restored" event.
     *
     * @param  \App\Outgoing  $outgoing
     * @return void
     */
    public function restored(Outgoing $outgoing)
    {
        //
    }

    /**
     * Handle the outgoing "force deleted" event.
     *
     * @param  \App\Outgoing  $outgoing
     * @return void
     */
    public function forceDeleted(Outgoing $outgoing)
    {
        //
    }
}
