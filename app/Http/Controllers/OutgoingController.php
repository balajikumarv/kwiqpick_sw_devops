<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Outgoing;
use App\Branch;
use App\IngredientCategory;
use App\Ingredient;
use App\IngredientUnit;
use Validator;

class OutgoingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
          
        $rules = array(
            'category_id.*' => 'required',
            'ingredient_id.*' => 'required',
            'quantity.*' => 'required',
            'unit_id.*' => 'required'
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        
        $record_number = uniqid();
        $branch_id = $request->branch_id;
        $category_id = $request->category_id;
        $ingredient_id = $request->ingredient_id;
        $quantity = $request->quantity;
        $unit_id = $request->unit_id;

        for ($count=0; $count < count($ingredient_id); $count++) { 
            $data = array(
                'record_number' => $record_number,
                'branch_id' =>  $branch_id,
                'category_id' => $category_id[$count],
                'ingredient_id' => $ingredient_id[$count],
                'quantity' => $quantity[$count],
                'unit_id' => $unit_id[$count],
                'created_at' => now()
            );
            Outgoing::create($data);
        }

        return response()->json([
            'success' => 'Data Added successfully.'
        ]);
      
      }
    }


    public function outgoinglist(Request $request)
    {
        if ($request->ajax()) {
          
        $rules = array(
            'category_id.*' => 'required',
            'ingredient_id.*' => 'required',
            'quantity.*' => 'required',
            'unit_id.*' => 'required'
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        
        $record_number = $request->record_number;
        $branch_id = $request->branch_id;
        $category_id = $request->category_id;
        $ingredient_id = $request->ingredient_id;
        $quantity = $request->quantity;
        $unit_id = $request->unit_id;

        for ($count=0; $count < count($ingredient_id); $count++) { 
            $data = array(
                'record_number' => $record_number,
                'branch_id' =>  $branch_id,
                'category_id' => $category_id[$count],
                'ingredient_id' => $ingredient_id[$count],
                'quantity' => $quantity[$count],
                'unit_id' => $unit_id[$count],
                'created_at' => now()
            );
            Outgoing::create($data);
        }

        return response()->json([
            'success' => 'Data Added successfully.'
        ]);
      
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $outgoing_id = $request['outgoing_id'];

        $outgoing_update=Outgoing::where('id', $outgoing_id)->first();

     $data_updated=[
         'ingredient_id'=>$request['edit_ingredient_id'],
         'quantity'=>$request['edit_quantity'],
         'unit_id'=>$request['edit_unit']
     ];
     $outgoing_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Outgoing::destroy($id);

        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function destroyIncoming($id)
    {

        Outgoing::find($id)->delete();

        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function ApprovalIncoming($id)
    {
        Outgoing::find($id)->update(['status' => 1]);

        return response()->json([
            'success' => 'Record has been Approval successfully!'
        ]);
    }

    public function AllOutgoing()
    {
        /*Outgoing::find(1)->delete();exit;*/
        // Outgoing::find(3)->update(['status' => 1]);exit;
        $this->middleware(['permission:Manage outgoing']);
        $outgoings = Outgoing::select('record_number','branch_id','created_at')->distinct()->get();
        $branches = Branch::orderBy('id', 'desc')->get();

        return view('admin_outgoing',['outgoings'=>$outgoings])
                    ->with("branches",$branches);

    }

    public function AddOutgoing()
    {
        
        $branches = Branch::orderBy('id', 'desc')->get();
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        return view('add_outgoing',['branches'=>$branches])
                    ->with('ingredient_categories', $ingredient_categories);

    }

    public function ViewOutgoing($id)
    {
        
        $view_outgoings = Outgoing::where('record_number', '=', $id)->get();
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $units = IngredientUnit::orderBy('id', 'desc')->get();
        return view('view_outgoing',['view_outgoings'=>$view_outgoings])
                    ->with('ingredient_categories',$ingredient_categories)
                    ->with('ingredients',$ingredients)
                    ->with('units',$units)
                    ->with('id',$id);

    }
    public function AddOutgoingList($id)
    {
        
        $add_outgoinglists = Outgoing::where('record_number', '=', $id)->take(1)->get();
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $units = IngredientUnit::orderBy('id', 'desc')->get();
        return view('add_outgoinglist',['add_outgoinglists'=>$add_outgoinglists])
                ->with('ingredient_categories', $ingredient_categories)
                ->with('ingredients', $ingredients)
                ->with('units', $units);

    }

    public function BranchIncomings()
    {
        
        /*$branch_incomings = Outgoing::where('status', '=', 0)->get();*/

        $branch_incomings = Outgoing::where('branch_id', auth()->user()->branch_id)->where('status','=', 0)->latest('id')->get();
        
        return view('branch_incomings',['branch_incomings'=>$branch_incomings]);

    }
}
