<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;
use App\IngredientCategory;
use App\IngredientUnit;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'category_id'=>$request['category_id'],
            'ingredient'=>$request['ingredient_name'],
            'unit_id'=>$request['unit_id'],
            'status'=>$request['ingredient_status']
        ];
        $ingredient = Ingredient::create($data);
        $ingredient->stocks()->create();
        return $ingredient;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $ingredient = $request['ingredient_id'];
        $ingredient_update=Ingredient::where('id', $ingredient)->first();

     $data_updated=[
         'category_id'=>$request['edit_category_id'],
         'ingredient'=>$request['edit_ingredient'],
         'unit_id'=>$request['edit_unit_id'],
         'status'=>$request['edit_status']
     ];
     $ingredient_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ingredient::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllIngredient()
    {
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredient_units = IngredientUnit::orderBy('id', 'desc')->get();

        return view('ingredient',['ingredients'=>$ingredients])
                ->with('ingredient_categories', $ingredient_categories)
                ->with('ingredient_units', $ingredient_units);

    }
}
