<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addon;
use App\Kitchen;

class AddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'name'=>$request['addon_name'],
            'price'=>$request['addon_amount'],
            'kitchen_id'=>$request['kitchen'],
            'status'=>$request['addon_status']
        ];
        return Addon::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $addon_id = $request['addon_id'];
        $addon_update=Addon::where('id', $addon_id)->first();

     $data_updated=[
         'name'=>$request['edit_addon'],
         'price'=>$request['edit_addon_amount'],
         'kitchen_id'=>$request['kitchen'],
         'status'=>$request['edit_status']
     ];
     $addon_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Addon::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllAddons()
    {
        $this->middleware(['permission:Manage addons|Manage branch addons']);
        $addons = Addon::when(auth()->user()->hasPermissionTo('Manage branch addons'), function($query){
            $query->whereHas('kitchen', function($query){
                $query->where('branch_id', auth()->user()->branch_id);
            });
        })->latest('id')->get();
        $kitchens = Kitchen::when(auth()->user()->hasPermissionTo('Manage branch addons'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->where('status','=', 1)->get();
        return view('admin_addon',['addons'=>$addons, 'kitchens' => $kitchens]);

    }

}
