<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incoming;
use Validator;
use App\Supplier;
use App\IngredientCategory;
use App\IngredientUnit;
use App\Ingredient;

class IncomingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->ajax()) {
          
        $rules = array(
            'ingredient_id.*' => 'required',
            'category_id.*' => 'required',
            'unit_id.*' => 'required',
            'quantity.*' => 'required',
            'rate.*' => 'required',
            'amount.*' => 'required',
            'tax.*' => 'required'
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        
        $record_number = uniqid();
        $bill_id = $request->billno;
        $supplier_id = $request->supplier_id;
        $ingredient_id = $request->ingredient_id;
        $category_id = $request->category_id;
        $unit_id = $request->unit_id;
        $quantity = $request->quantity;
        $rate = $request->rate;
        $amount = $request->amount;
        $tax = $request->tax;

        for ($count=0; $count < count($ingredient_id); $count++) { 
            $data = array(
                'record_number' => $record_number,
                'bill_id' =>  $bill_id,
                'supplier_id' => $supplier_id,
                'ingredient_id' => $ingredient_id[$count],
                'category_id' => $category_id[$count],
                'unit_id' => $unit_id[$count],
                'quantity' => $quantity[$count],
                'rate' => $rate[$count],
                'amount' => $amount[$count],
                'tax' => $tax[$count],
                'created_at' => now()
            );
            Incoming::create($data);
        }
        return response()->json([
            'success' => 'Data Added successfully.'
        ]);
      
      }
    }
    public function incominglist(Request $request)
    {

        if ($request->ajax()) {
          
        $rules = array(
            'ingredient_id.*' => 'required',
            'category_id.*' => 'required',
            'unit_id.*' => 'required',
            'quantity.*' => 'required',
            'rate.*' => 'required',
            'amount.*' => 'required',
            'tax.*' => 'required'
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        
        $record_number = $request->record_number;
        $bill_id = $request->billno;
        $supplier_id = $request->supplier_id;
        $ingredient_id = $request->ingredient_id;
        $category_id = $request->category_id;
        $unit_id = $request->unit_id;
        $quantity = $request->quantity;
        $rate = $request->rate;
        $amount = $request->amount;
        $tax = $request->tax;

        for ($count=0; $count < count($ingredient_id); $count++) { 
            $data = array(
                'record_number' => $record_number,
                'bill_id' =>  $bill_id,
                'supplier_id' => $supplier_id,
                'ingredient_id' => $ingredient_id[$count],
                'category_id' => $category_id[$count],
                'unit_id' => $unit_id[$count],
                'quantity' => $quantity[$count],
                'rate' => $rate[$count],
                'amount' => $amount[$count],
                'tax' => $tax[$count],
                'created_at' => now()
            );
            Incoming::create($data);
        }

        return response()->json([
            'success' => 'Data Added successfully.'
        ]);
      
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $income_id = $request['income_id'];

        $income_update=Incoming::where('id', $income_id)->first();

     $data_updated=[
         'record_number'=>$request['edit_record_no'],
         'bill_id'=>$request['edit_bill_no'],
         'supplier_id'=>$request['edit_supplier_id'],
         'ingredient_id'=>$request['edit_ingredient'],
         'category_id'=>$request['edit_category_id'],
         'unit_id'=>$request['edit_unit'],
         'quantity'=>$request['edit_quantity'],
         'rate'=>$request['edit_rate'],
         'amount'=>$request['edit_amount'],
         'tax'=>$request['edit_tax']
     ];
     $income_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Incoming::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllIncome()
    {
        /*$incoms = Incoming::orderBy('id', 'desc')->get();*/
        /*$incoms = Incoming::distinct()->get(['record_number']);*/
         $incoms = Incoming::select('record_number','supplier_id', 'bill_id', 'created_at')->distinct()->get();

        return view('admin_incoming',['incoms'=>$incoms]);

    }

    public function AddIncome()
    {
        
        $suppliers = Supplier::orderBy('id', 'desc')->get();
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();

        return view('add_income',['suppliers'=>$suppliers])
                ->with('ingredient_categories', $ingredient_categories);

    }
    public function AddIncomingList($id)
    {
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $units = IngredientUnit::orderBy('id', 'desc')->get();
        $add_incominglists = Incoming::where('record_number', '=', $id)->take(1)->get();
        return view('add_incomelist',['add_incominglists'=>$add_incominglists])
                ->with('ingredient_categories', $ingredient_categories)
                    ->with('ingredients', $ingredients)
                    ->with('units', $units);

    }

    public function ViewIncoming($id)
    {
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $units = IngredientUnit::orderBy('id', 'desc')->get();
        $view_incoms = Incoming::where('record_number', '=', $id)->get();
        return view('view_incomings',['view_incoms'=>$view_incoms])
                    ->with('ingredient_categories', $ingredient_categories)
                    ->with('ingredients', $ingredients)
                    ->with('units', $units)
                    ->with('id',$id);

    }

    public function getIngredientList(Request $request)
    {
        $category_id = $request['category_id'];
        $ingredients=Ingredient::where('category_id', $category_id)->get();

        return response()->json($ingredients);
    }
    public function getUnitList(Request $request)
    {
        $ingredient_id = $request['ingredient_id'];
        $ingredient_units=Ingredient::where('id', $ingredient_id)->get();
        $unit_id = $ingredient_units[0]['unit_id'];
        
        $units=IngredientUnit::where('id', $unit_id)->get();

        return response()->json($units);
    }

} 
