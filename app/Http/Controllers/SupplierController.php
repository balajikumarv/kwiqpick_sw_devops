<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = [
            'name' => $request['supplier_name'],
            'address'=> $request['supplier_address'],
            'gst' => $request['supplier_gst'],
            'number' => $request['supplier_number'],
            'alternate_number'=> $request['supplier_alternate_number'],
            'email'=> $request['supplier_email'],
            'contact_name'=> $request['supplier_contact_name']
        ];
        $supplier = Supplier::create($data);
        return $supplier;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $supplier_id = $request['supplier_id'];
        $supplier_update = Supplier::where('id', $supplier_id)->first();

        $data_updated=[
            'name'=>$request['edit_supplier_name'],
            'address'=>$request['edit_supplier_address'],
            'gst'=>$request['edit_supplier_gst'],
            'number'=>$request['edit_supplier_number'],
            'alternate_number'=>$request['edit_supplier_alternate_number'],
            'email'=>$request['edit_supplier_email'],
            'contact_name'=>$request['edit_supplier_contact_name']
        ];
        $supplier_update->update($data_updated);

         return response()->json(['success' => 'Record has been Updated successfully!']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supplier::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllSupplier()
    {
        $suppliers = Supplier::orderBy('id', 'desc')->get();

        return view('admin_supplier',['suppliers'=>$suppliers]);

    }
}
