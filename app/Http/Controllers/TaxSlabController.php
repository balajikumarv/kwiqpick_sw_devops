<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TaxSlab;

class TaxSlabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'tax_slab'=>$request['tax_slab_name'],
            'status'=>$request['tax_slab_status']
        ];
        return TaxSlab::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $tax_slab = $request['tax_slab_id'];
        $tax_slab_update=TaxSlab::where('id', $tax_slab)->first();

     $data_updated=[
         'tax_slab'=>$request['edit_tax_slab'],
         'status'=>$request['edit_status']
     ];
     $tax_slab_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TaxSlab::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllTaxSlab()
    {
        $tax_slabs = TaxSlab::orderBy('id', 'desc')->get();
        return view('admin_tax_slab',['tax_slabs'=>$tax_slabs]);

    }
}
