<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Branch;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'category'=>$request['category_name'],
            'branch_id'=>$request['category_branch'],
            'status'=>$request['category_status']
        ];
        return Category::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $category = $request['category_id'];
        $category_update=Category::where('id', $category)->first();

     $data_updated=[
         'category'=>$request['edit_category'],
         'branch_id'=>$request['edit_category_branch'],
         'status'=>$request['edit_status']
     ];
     $category_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllCategory()
    {
        $this->middleware(['permission:Manage category|Manage branch category']);
        $categories = Category::when(auth()->user()->hasPermissionTo('Manage branch category'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->latest('id')->get();
        $branches = Branch::when(auth()->user()->hasPermissionTo('Manage branch category'), function($query){
            $query->where('id', auth()->user()->branch_id);
        })->where('status', 1)->get();
        return view('admin_category',['categories'=>$categories])
                ->with("branches",$branches);

    }
}
