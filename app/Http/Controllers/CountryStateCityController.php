<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/*use App\Http\Requests;
use DB;*/
use App\Country;
use App\State;
use App\City;

class CountryStateCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        $countries = DB::table("countries")->lists("name","id");
        return view('index',compact('countries'));
    }*/

    public function getStateList(Request $request)
    {
        $country_id = $request['country_id'];
        $states=State::where('country_id', $country_id)->get();

        return response()->json($states);
    }

    public function getCityList(Request $request)
    {
        $state_id = $request['state_id'];
        $cities=City::where('state_id', $state_id)->get();
        /*$cities = DB::table("cities")
                    ->where("state_id",$request->state_id)
                    ->lists("name","id");*/
        return response()->json($cities);
    }

}
