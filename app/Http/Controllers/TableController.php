<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Table;
use App\Zone;
use App\Branch;
class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vendar_per = $request['vendar_percentage'];
        if ($vendar_per=='') {
            # code...
            $vendar_value = '0';
        }
        else{
            
            $vendar_value = $vendar_per;
        }

        $data=[
            'name'=>$request['table_name'],
            'table_capacity'=>$request['table_capacity'],
            'branch_id'=>$request['table_branch'],
            'zone_id'=>$request['table_zone'],
            'table_vendar'=>$request['table_vendar'],
            'vendar_percentage'=>$vendar_value,
            'status'=>$request['table_status']
        ];
        return Table::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $table_id = $request['table_id'];
        $table_update=Table::where('id', $table_id)->first();

        $vendar_per_edit = $request['edit_vendar_percentage'];
        $vendar_tab_value = $request['edit_table_vendar'];

        if ($vendar_per_edit=='') {
            # code...
            $vendar_per_edit = '0';
        }
        else{
            if ($vendar_tab_value==2) {
                # code...
                $vendar_per_edit = $vendar_per_edit;
            }else{
                $vendar_per_edit = "0";
            }
            
        }

     $data=[
         'name'=>$request['edit_table_name'],
         'table_capacity'=>$request['edit_table_capacity'],
         'branch_id'=>$request['edit_table_branch'],
         'zone_id'=>$request['edit_table_zone'],
         'table_vendar'=>$vendar_tab_value,
         'vendar_percentage'=>$vendar_per_edit,
         'status'=>$request['edit_table_status']
     ];
     $table_update->update($data);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Table::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllTable()
    {
        $this->middleware(['permission:Manage tables|Manage branch tables']);
        $tables = Table::when(auth()->user()->hasPermissionTo('Manage branch tables'), function($query){
            $query->whereHas('zone', function($query){
                $query->where('branch_id', auth()->user()->branch_id);
            });
        })->latest('id')->get();
        
        $zones = Zone::when(auth()->user()->hasPermissionTo('Manage branch tables'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->where('status','=', 1)->get();

        $branches = Branch::orderBy('id', 'desc')->get();

        return view('admin_table',['tables'=>$tables])
                ->with("zones",$zones)
                ->with("branches",$branches);

    }

    public function getZoneList(Request $request)
    {
        $branch_id = $request['branch_id'];
        $zones=Zone::where('branch_id', $branch_id)->get();

        return response()->json($zones);
    }
}
