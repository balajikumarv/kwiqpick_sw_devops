<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;
use App\Category;
use Validator;
use App\Menu;
use App\IngredientCategory;
use App\IngredientUnit;
use App\Ingredient;
 
class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
          
        $rules = array(
            'category_id.*' => 'required',
            'ingredient_id.*' => 'required',
            'unit_id.*' => 'required',
            'quantity.*' => 'required'
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        
        $recipe_id = uniqid();
        $menu_category_id = $request->menu_category_id;
        $menu_id = $request->menu_id;
        $ingredient_id = $request->ingredient_id;
        $category_id = $request->category_id;
        $quantity = $request->quantity;
        $unit_id = $request->unit_id;

        for ($count=0; $count < count($ingredient_id); $count++) { 
            $data = array(
                'recipe_id' => $recipe_id,
                'menu_category_id' =>  $menu_category_id,
                'menu_id' => $menu_id,
                'ingredient_id' => $ingredient_id[$count],
                'category_id' => $category_id[$count],
                'quantity' => $quantity[$count],
                'unit_id' => $unit_id[$count],
                'created_at' => now()
            );
            $insert_data[] = $data;
        }

        Recipe::insert($insert_data);
        return response()->json([
            'success' => 'Data Added successfully.'
        ]);
      
      }


    }

    public function RecipeList(Request $request)
    {

        if ($request->ajax()) {
          
        $rules = array(
            'category_id.*' => 'required',
            'ingredient_id.*' => 'required',
            'unit_id.*' => 'required',
            'quantity.*' => 'required'
        );
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json([
                'error' => $error->errors()->all()
            ]);
        }
        
        $recipe_id = $request->recipe_id;
        $menu_category_id = $request->menu_category_id;
        $menu_id = $request->menu_id;
        $ingredient_id = $request->ingredient_id;
        $category_id = $request->category_id;
        $quantity = $request->quantity;
        $unit_id = $request->unit_id;

        for ($count=0; $count < count($ingredient_id); $count++) { 
            $data = array(
                'recipe_id' => $recipe_id,
                'menu_category_id' =>  $menu_category_id,
                'menu_id' => $menu_id,
                'ingredient_id' => $ingredient_id[$count],
                'category_id' => $category_id[$count],
                'quantity' => $quantity[$count],
                'unit_id' => $unit_id[$count],
                'created_at' => now()
            );
            $insert_data[] = $data;
        }

        Recipe::insert($insert_data);
        return response()->json([
            'success' => 'Data Added successfully.'
        ]);
      
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $recipe_id = $request['recipe_id'];

        $recipe_update=Recipe::where('id', $recipe_id)->first();

     $data_updated=[
         'ingredient_id'=>$request['edit_ingredient_id'],
         'category_id'=>$request['edit_category_id'],
         'quantity'=>$request['edit_quantity'],
         'unit_id'=>$request['edit_unit'],
         'updated_at' => now()
     ];
     $recipe_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Recipe::destroy($id);

        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllRecipe()
    {
        $recipes = Recipe::select('recipe_id','menu_category_id', 'menu_id', 'created_at')->distinct()->get();
        return view('admin_recipe',['recipes'=>$recipes]);

    }

    public function AddRecipe()
    {
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $units = IngredientUnit::orderBy('id', 'desc')->get();
        $item_categories = Category::where('branch_id', auth()->user()->branch_id)->orderBy('id', 'desc')->get();
        return view('add_recipe',['item_categories'=>$item_categories])
                ->with('ingredient_categories', $ingredient_categories)
                ->with('ingredients', $ingredients)
                ->with('units', $units);

    }

    public function getItemList(Request $request)
    {
        $category_id = $request['category_id'];
        /*$categories = Category::when(auth()->user()->hasPermissionTo('Manage branch items'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->where('status','=', 1)->latest('id')->get();*/

        $tables=Menu::when(auth()->user()->hasPermissionTo('Manage branch tables'), function($query){
                $query->where('branch_id', auth()->user()->branch_id);
        })->where('category_id', $category_id)->get();

        return response()->json($tables);
    }

    public function ViewRecipe($id)
    {
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $units = IngredientUnit::orderBy('id', 'desc')->get();
        $view_recipes = Recipe::where('recipe_id', '=', $id)->get();
        return view('view_recipe',['view_recipes'=>$view_recipes])
                    ->with('ingredient_categories',$ingredient_categories)
                    ->with('ingredients',$ingredients)
                    ->with('units',$units)
                    ->with('id',$id);

    }
    public function AddRecipeList($id)
    {
        $item_categories = Category::where('branch_id', auth()->user()->branch_id)->orderBy('id', 'desc')->get();
        $ingredient_categories = IngredientCategory::orderBy('id', 'desc')->get();
        $ingredients = Ingredient::orderBy('id', 'desc')->get();
        $units = IngredientUnit::orderBy('id', 'desc')->get();
        $add_recipelists = Recipe::where('recipe_id', '=', $id)->take(1)->get();
        return view('add_recipelist',['add_recipelists'=>$add_recipelists])
                ->with('ingredient_categories', $ingredient_categories)
                ->with('ingredients', $ingredients)
                ->with('units', $units)
                ->with('item_categories', $item_categories);
    }
}
