<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IngredientUnit;

class IngredientUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'unit'=>$request['unit_name'],
            'status'=>$request['unit_status']
        ];
        return IngredientUnit::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $unit = $request['unit_id'];
        $unit_update=IngredientUnit::where('id', $unit)->first();

     $data_updated=[
         'unit'=>$request['edit_unit'],
         'status'=>$request['edit_status']
     ];
     $unit_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        IngredientUnit::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllIngredientUnit()
    {
        $units = IngredientUnit::orderBy('id', 'desc')->get();

        return view('ingredient_unit',['units'=>$units]);

    }
}
