<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\Branch;
use App\User;
use Hash;
class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $img_id = $request['staff_id_number'];

            if( $request->hasFile('staff_image')) {
                $image = $request->file('staff_image');
                $path = public_path(). '/images/staff_images/';
                $RandomImageNumber = uniqid();
                $filename = $img_id . '_' . time() . $RandomImageNumber . '.' . $image->getClientOriginalExtension();
                $image->move($path, $filename);

                
            }
        $data = [
            'name' => $request['staff_name'],
            'email'=> $request['staff_email'],
            'username' => $request['staff_user_name'],
            'email_verified_at' => now(),
            'password'=> Hash::make($request['staff_password']),
            'role'=> $request['staff_role'],
            'branch_id'=> $request['staff_branch'],
            'number'=> $request['staff_number'],
            'alternate_number'=>$request['staff_alternate_number'],
            'id_type'=> $request['staff_id_type'],
            'id_number'=> $request['staff_id_number'],
            'address'=> $request['staff_address'],
            'bank_name'=> $request['staff_bank_name'],
            'bank_account_number'=> $request['staff_bank_account_number'],
            'bank_ifsc'=> $request['staff_bank_ifsc'],
            'status'=> $request['staff_status'],
            'image'=>$filename
        ];
        $user = User::create($data);
        $user->assignRole($request['staff_role']);
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $staff_id = $request['staff_id'];
        $user = User::find($staff_id);
        $image_name=$user->image;

        if( $request->hasFile('edit_staff_image')) {
           if($image_name)
           {
             unlink(public_path(). '/images/staff_images/'.$image_name);
           }
             $image = $request->file('edit_staff_image');
             $path = public_path(). '/images/staff_images/';
             $RandomImageNumber = uniqid();
             $filename = time() . $RandomImageNumber . '.' . $image->getClientOriginalExtension();
             $image->move($path, $filename);
             $image_name=$filename;
         }

     $data=[
            'name' => $request['edit_staff_name'],
            'email'=> $request['edit_staff_email'],
            'username' => $request['edit_staff_user_name'],
            'email_verified_at' => now(),
            'role'=> $request['edit_staff_role'],
            'branch_id'=> $request['edit_staff_branch'],
            'number'=> $request['edit_staff_number'],
            'alternate_number'=>$request['edit_staff_alternate_number'],
            'id_type'=> $request['edit_staff_id_type'],
            'id_number'=> $request['edit_staff_id_number'],
            'address'=> $request['edit_staff_address'],
            'bank_name'=> $request['edit_staff_bank_name'],
            'bank_account_number'=> $request['edit_staff_bank_account_number'],
            'bank_ifsc'=> $request['edit_staff_bank_ifsc'],
            'status'=> $request['edit_staff_status'],
            'image'=>$image_name
     ];

     if($request->has('edit_staff_password')){
        $data['staff_password'] = $request['edit_staff_password'];
        User::where('id', $staff_id)->update(['password' => Hash::make($request['edit_staff_password'])]);
     }
     $user->syncRoles($request->only(['edit_staff_role']));
     $user->update($data);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllStaff()
    {
        $this->middleware(['permission:Manage users|Manage branch users']);
        /*$staffs = Staff::orderBy('id', 'desc')->get();*/
        $staffs = User::when(auth()->user()->hasPermissionTo('Manage branch users'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->latest('id')->with('branch')->get();
        $branches = Branch::when(auth()->user()->hasPermissionTo('Manage branch items'), function($query){
            $query->where('id', auth()->user()->branch_id);
        })->where('status', 1)->get();
        return view('admin_staff',['staffs'=>$staffs])->with("branches",$branches);

    }
}
