<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imanagement;

class ImanagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'name' => $request['imanagement_name'],
            'unit'=> $request['imanagement_unit'],
            'quantity' => $request['imanagement_quantity'],
            'category' => $request['imanagement_category']
        ];
        $imanagement = Imanagement::create($data);
        return $imanagement;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $itemmanagement_id = $request['itemmanagement_id'];
        $itemmanagement_update = Imanagement::where('id', $itemmanagement_id)->first();

        $data_updated=[
            'name'=>$request['edit_imanagement_name'],
            'unit'=>$request['edit_imanagement_unit'],
            'quantity'=>$request['edit_imanagement_quantity'],
            'category'=>$request['edit_imanagement_category']
        ];
        $itemmanagement_update->update($data_updated);

         return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Imanagement::destroy($id);

        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllImanagement()
    {
        $itemmanagements = Imanagement::orderBy('id', 'desc')->get();

        return view('admin_item_management',['itemmanagements'=>$itemmanagements]);

    }
}
