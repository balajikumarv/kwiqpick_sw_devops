<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Branch;
use App\Category;
use App\TaxSlab;
use App\Kitchen;
use App\Addon;
use App\Tax;
use App\Cuisine;
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $half_p = $request['half_plate'];
        $family_p = $request['familypack_plate'];
        
        //Half Plate
        if ($half_p=="") {
            $half = 0;
        }else{
            $half = $half_p;
        }

        //Family Plate
        if ($family_p=="") {
            $family = 0;
        }else{
            $family = $family_p;
        }

        $data=[
            'menu_type'=>$request['menu_type'],
            'branch_id'=>$request['menu_branch'],
            'category_id'=>$request['menu_category'], 
            'menu_desc'=>$request['menu_desc'],
            'name'=>$request['menu_item'],
            'plate'=>$request['menu_plate'],
            'price'=>$request['menu_price'],
            'half_plate'=>$half,
            'familypack_plate'=>$family,
            'menu_cuisine'=> $request->input('menu_cuisine', ''),
            'menu_incl_excl'=>$request['menu_incl_excl'],
            'menu_item_spicy_level'=>$request['menu_item_spicy_level'],
            'kitchen_id'=>$request['menu_kitchen'],
            'menu_item_veg_nonveg'=>$request['menu_item_veg_nonveg'],
            'menu_status'=>$request['menu_status'],
            'sgst'=>$request['sgst'],
            'cgst'=>$request['cgst'],
            'short_code' => $request->short_code
        ];
        $menu = Menu::create($data);
        $menu->addons()->attach($request['menu_addons']);
        $menu->taxes()->attach($request['menu_tax']);
        return $menu;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $plate_p = $request['edit_menu_plate'];
        $half_plate_e = $request['edit_half_plate'];
        $familypack_plate_e = $request['edit_familypack_plate'];
        if ($plate_p == '0') {
            $h_plate = 0;
            $f_plate = 0;
        }else{
            $h_plate = $half_plate_e;
            $f_plate = $familypack_plate_e;
        }
        $menu_id = $request['menu_id'];
        $menu = Menu::where('id', $menu_id)->firstOrFail();
     $data_update=[
         'menu_type'=>$request['edit_menu_type'],
         'branch_id'=>$request['edit_menu_branch'],
         'category_id'=>$request['edit_menu_category'],
         'menu_desc'=>$request['edit_menu_desc'],
         'name'=>$request['edit_menu_item'],
         'plate'=>$plate_p,
         'price'=>$request['edit_menu_price'],
         'half_plate'=>$h_plate,
         'familypack_plate'=>$f_plate,
         'menu_cuisine'=> $request->input('edit_menu_cuisine', ''),
         'menu_incl_excl'=>$request['edit_menu_incl_excl'],
         'menu_item_spicy_level'=>$request['edit_menu_item_spicy_level'],
         'kitchen_id'=>$request['edit_menu_kitchen'],
         'menu_item_veg_nonveg'=>$request['edit_menu_item_veg_nonveg'],
         'menu_status'=>$request['edit_menu_status'],
         'sgst'=>$request['sgst'],
         'cgst'=>$request['cgst'],
         'short_code' => $request->short_code
     ];
     $menu->update($data_update);
     $menu->addons()->sync($request['edit_menu_addons']);
     $menu->taxes()->sync($request['edit_menu_tax']);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllMenu()
    {
        $this->middleware(['permission:Manage items|Manage branch items']);

        $menus = Menu::when(auth()->user()->hasPermissionTo('Manage branch items'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->latest('id')->get();
        
        $branches = Branch::when(auth()->user()->hasPermissionTo('Manage branch items'), function($query){
            $query->where('id', auth()->user()->branch_id);
        })->where('status', 1)->latest('id')->get();

        $categories = Category::when(auth()->user()->hasPermissionTo('Manage branch items'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->where('status','=', 1)->latest('id')->get();
        $taxslabs = TaxSlab::where('status','=', 1)->latest('id')->get();
        $addons = Addon::when(auth()->user()->hasPermissionTo('Manage branch items'), function($query){
            $query->whereHas('kitchen', function($query){
                $query->where('branch_id', auth()->user()->branch_id);
            });
        })->where('status','=', 1)->latest('id')->get();
        $tax = Tax::where('status','=', 1)->latest('id')->get();
        
        $kitchens = Kitchen::when(auth()->user()->hasPermissionTo('Manage branch items'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->where('status','=', 1)->latest('id')->get();
        $cuisines = Cuisine::orderBy('id', 'desc')->get();
        return view('admin_menu',['menus'=>$menus])
                ->with("branches",$branches)
                ->with("categories",$categories)
                ->with("taxslabs",$taxslabs)
                ->with("kitchens",$kitchens)
                ->with("addons",$addons)
                ->with("taxes",$tax)
                ->with("cuisines", $cuisines);

    }

    public function getCategoryList(Request $request)
    {
        $branch_id = $request['branch_id'];
        $categories=Category::where('branch_id', $branch_id)->get();
        $kitchens=Kitchen::where('branch_id', $branch_id)->get();

        /*return response()->json($categories);*/

        return response()->json(array(
            'kitchens' => $kitchens,
            'categories' => $categories,
        ));
    }
}
