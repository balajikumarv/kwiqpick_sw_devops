<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = [
        'zone_id', 'table_id', 'user_id', 'branch_id', 'quantity', 'sub_total', 'net_total', 'cgst', 'sgst', 'invoice_no', 'status', 'payment_type'
    ];

    public function items() {
        return $this->hasMany('\App\BillItem');
    }
    public function kitchenOrders() {
        return $this->hasMany('\App\KitchenOrder');
    }
    public function table() {
        return $this->belongsTo('\App\Table');
    }
    public function branch(){
      return $this->belongsTo('App\Branch');
    }
}
