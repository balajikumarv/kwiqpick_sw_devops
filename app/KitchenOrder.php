<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KitchenOrder extends Model
{
    protected $fillable = [
        'zone_id', 'table_id', 'user_id', 'branch_id', 'bill_id', 'quantity', 'sub_total', 'net_total', 'cgst', 'sgst', 'invoice_no', 'status', 'kot_status', 'payment_type'
    ];

    public function items() {
        return $this->hasMany('\App\KitchenOrderItem');
    }
    public function table() {
        return $this->belongsTo('\App\Table');
    }
    public function branch(){
      return $this->belongsTo('App\Branch');
    }
    public function bill(){
      return $this->belongsTo('App\Bill');
    }
}
