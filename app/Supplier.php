<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'id', 'name', 'address', 'gst', 'number', 'alternate_number', 'email','contact_name'
    ];

    public function incoming()
    {
        return $this->hasMany('\App\Incoming');
    }
}
