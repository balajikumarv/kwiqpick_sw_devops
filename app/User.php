<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'username', 'email_verified_at', 'role' , 'branch_id', 'number', 'alternate_number', 'id_type', 'id_number', 'address', 'bank_name', 'bank_account_number', 'bank_ifsc', 'status', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function bills() {
        return $this->hasMany('\App\Bill');
    }

    public function kitchenOrders() {
        return $this->hasMany('\App\KitchenOrder');
    }

    public function branch(){
      return $this->belongsTo('App\Branch');
    }
}
