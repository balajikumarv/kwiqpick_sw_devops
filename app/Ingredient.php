<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = [
        'id', 'category_id', 'ingredient', 'unit_id', 'status'
     ];

     public function incoming()
    {
        return $this->hasMany('\App\Incoming');
    }

    public function outgoing()
    {
        return $this->hasMany('\App\Outgoing');
    }

    public function recipe()
    {
        return $this->hasMany('\App\Recipe');
    }

    public function stocks(){
        return $this->hasMany('\App\Stock');
    }
}
