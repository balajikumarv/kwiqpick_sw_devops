<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addon extends Model
{
    protected $fillable = [
        'id', 'name', 'price', 'status', 'kitchen_id'
    ];

    protected $appends = ['original_price', 'net_price'];

    public function menus() {
        return $this->belongsToMany('App\Menu','menu_addons');
    }
    public function kitchen() {
        return $this->belongsTo('App\Kitchen');
    }
    public function getcgstAttribute(){
        return 0;
    }
    public function getsgstAttribute(){
        return 0;
    }
    public function getoriginalPriceAttribute(){
        return $this->attributes['price'];
    }
    public function getcgstPriceAttribute(){
        return 0;
    }
    public function getsgstPriceAttribute(){
        return 0;
    }
    public function getnetPriceAttribute(){
        return $this->attributes['price'];
    }
}
