<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $fillable = [
        'id', 'category', 'branch_id', 'status'
     ];

    public function menus(){       
        return $this->hasMany('App\Menu');
    }
    
    public function branch(){
      return $this->belongsTo('App\Branch');
    }

    public function recipe()
    {
        return $this->hasMany('\App\Recipe');
    }
}
