<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $fillable = [
        'id', 'name', 'branch_id', 'status'
    ];

    public function branch(){
      return $this->belongsTo('App\Branch');
    }

    public function tables() {
      return $this->hasMany('\App\Table');
    }
}
