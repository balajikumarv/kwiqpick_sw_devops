<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        'id', 'branch_id', 'branch_name', 'branch_number', 'branch_google_map', 'branch_address','branch_country','branch_state','branch_city', 'branch_image', 'branch_manager_name', 'branch_manager_number', 'printer_name', 'status'
    ];

    public function bills()
    {
    	return $this->hasMany('\App\Bill');
    }

    public function zones()
    {
        return $this->hasMany('\App\Zone');
    }

    public function menus()
    {
        return $this->hasMany('\App\Menu');
    }

    public function categories()
    {
        return $this->hasMany('\App\Category');
    }

    public function kitchens()
    {
        return $this->hasMany('\App\Kitchen');
    }
	
	public function kitchenOrders() {
		return $this->hasMany('\App\KitchenOrder');
	}

    public function outgoing() {
        return $this->hasMany('\App\Outgoing');
    }
}
