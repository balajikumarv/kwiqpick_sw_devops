<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientCategory extends Model
{
    protected $fillable = [
        'id', 'category', 'status'
     ];

     public function incoming()
    {
        return $this->hasMany('\App\Incoming');
    }

    public function outgoing()
    {
        return $this->hasMany('\App\Outgoing');
    }

    public function recipe()
    {
        return $this->hasMany('\App\Recipe');
    }
}
