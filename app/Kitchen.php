<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kitchen extends Model
{
   protected $fillable = [
      'id', 'name', 'branch_id', 'printer_name', 'chef', 'status'
   ];

   public function branch(){
    	
      return $this->belongsTo('App\Branch');
  }
}