<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incoming extends Model
{
  protected $fillable = [
    'id', 'record_number', 'bill_id', 'supplier_id', 'ingredient_id', 'category_id', 'unit_id', 'quantity', 'rate', 'amount', 'tax'
  ];

  public function supplier()
  {
    return $this->belongsTo('App\Supplier');
  }

  public function ingredient()
  {
    return $this->belongsTo('App\Ingredient');
  }

  public function category()
  {
    return $this->belongsTo('App\IngredientCategory');
  }

  public function unit()
  {
    return $this->belongsTo('App\IngredientUnit');
  }
}
