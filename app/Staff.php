<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = [
        'id', 'staff_name', 'staff_role', 'branch_id', 'staff_user_name', 'staff_password', 'staff_email', 'staff_number', 'staff_alternate_number', 'staff_id_type', 'staff_id_number', 'staff_address', 'staff_bank_name', 'staff_bank_account_number', 'staff_bank_ifsc', 'staff_status', 'user_id', 'image'
    ];

    public function branch(){
      return $this->belongsTo('App\Branch');
  }
}