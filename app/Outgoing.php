<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outgoing extends Model
{
    protected $fillable = [
        'id', 'record_number', 'branch_id', 'category_id', 'ingredient_id', 'quantity', 'unit_id', 'status'
    ];

    public function branch(){
      return $this->belongsTo('App\Branch');
  }

    public function ingredient(){
      return $this->belongsTo('App\Ingredient');
    }

    public function category(){
      return $this->belongsTo('App\IngredientCategory');
    }

    public function unit(){
      return $this->belongsTo('App\IngredientUnit');
    }
}
