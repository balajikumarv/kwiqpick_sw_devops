<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillTitle extends Model
{
    protected $fillable = [
        'id', 'title'
    ];
}
