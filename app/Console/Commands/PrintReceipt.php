<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\UriPrintConnector;
use App\Bill;

class PrintReceipt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'print:receipt {bill : The ID of the bill}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $billId = $this->argument('bill');
        $bill = Bill::findOrFail($billId);
        $kitchenIds = $bill->kitchenOrders()->pluck('id')->map(function($kitchenId){
            return sprintf('%05d', $kitchenId);
        })->implode(', ');
        // return true;
        try {
            // Enter the share name for your USB printer here
            // $connector = null;
            $connector = UriPrintConnector::get($bill->branch->printer_name);

            /* Print a "Hello world" receipt" */
            $printer = new Printer($connector);
            // /* Initialize */
            $printer->initialize();
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->setTextSize(2,1);
            $printer->text(str_replace('\\n', "\n", config('app.print_app_name'))."\n");

            $printer->selectPrintMode(); // Reset
            $printer->setTextSize(1, 1);
            $printer->text("\n");

            $printer->text($bill->branch->branch_address."\n");
            $printer->text("GST:".config('app.gst_number')."\n");

            $printer->text("\n");
            $printer->setJustification(Printer::JUSTIFY_LEFT);

            $printer->text("-----------TAX INVOICE----------\n\n");
            $printer->text("BILL NO : ".sprintf('%05d', $bill->id)."  TBL : {$bill->table->name}\n");
            $printer->text("KOT NOS : $kitchenIds\n");

            $printer->text("\n");
            $printer->text("--------------------------------\n");
            $printer->text("ITEM NAME\n");
            $printer->text("      QTY    PRICE      AMT\n");
            $printer->text("--------------------------------\n");
            
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $bill->items->each(function($item) use($printer) {
                $this->printMenu($printer, $item);
            });

            $printer->text("\n");
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("--------------------------------\n");
            
            $printer->text("SUB TOTAL : RS. $bill->sub_total\n");
            $printer->text("CGST      : RS. $bill->cgst\n");
            $printer->text("SGST      : RS. $bill->sgst\n");
            
            $printer->text("--------------------------------\n");
            $printer->selectPrintMode(); // Reset
            $printer->setTextSize(2,1);
            $printer->text("NET TOTAL\n");
            $printer->text("RS. $bill->net_total\n");
            $printer->selectPrintMode(); // Reset
            $printer->text("--------------------------------\n");
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("Thank You\n");
            $printer->text($bill->created_at->format('d M Y h:i:s A')."\n");
            $printer->feed(4);
            $printer->cut();
            $printer->close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }
    function printMenu(Printer $printer, $menuItem)
    {
        $printer->text("{$menuItem->name}\n");
        $printer->text("      $menuItem->quantity     $menuItem->price     $menuItem->total_amount\n");
    }
}