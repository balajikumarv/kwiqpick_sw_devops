<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\UriPrintConnector;
use App\Kitchen;
use App\KitchenOrder;
use App\KitchenOrderItem;

class PrintKitchenCancelledReceipt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'print:kitchenCancelled {itemIds : The ID list of the KOT items}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $removeKotItems = explode(',', $this->argument('itemIds'));
        $kotItemsAll = KitchenOrderItem::whereIn('id', $removeKotItems)->get()->groupBy(['kitchen_order_id', 'kitchen_id']);
        $kotItemsAll->each(function($kotItems, $kotId){
            $kot = KitchenOrder::findOrFail($kotId);
            $kotItems->each(function($items, $kitchenId) use($kot) {
                $this->printKitchenItems($kot, Kitchen::find($kitchenId), $items);
            });
        });
    }

    public function printKitchenItems($kot, $kitchen, $items)
    {
        try {
            $connector = UriPrintConnector::get($kitchen->printer_name);
            $printer = new Printer($connector);
            // /* Initialize */
            $printer->initialize();
            $printer->feed(1);
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("--------------------------------\n");
            $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);

            $printer->text("KOT CANCELLED\n");
            $printer -> selectPrintMode();
            $printer->text("--------------------------------\n");
            $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text("KOT : ".sprintf('%05d', $kot->id)."\nTBL : {$kot->table->name}\n");

            $printer->text("\n");
            $printer -> selectPrintMode();
            $printer->text("--------------------------------\n");
            
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $items->each(function($item) use($printer) {
                $printer->text("{$item->quantity}     $item->name\n");
            });
            $printer->text("--------------------------------\n");
            $printer->text("--------------------------------\n");
            $printer->feed(3);
            $printer->cut();
            $printer->close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }
}
