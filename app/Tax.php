<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $fillable = [
        'id', 'category', 'percentage', 'status'
    ];

    public function menus() {
        return $this->belongsToMany('App\Menu','menu_tax');
    }
}
