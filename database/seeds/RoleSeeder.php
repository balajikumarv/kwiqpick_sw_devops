<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = App\User::where(['username' => 'admin'])->first();
        $manager = App\User::where(['username' => 'manager'])->first();
        $staff = App\User::where(['username' => 'staff'])->first();
        $chef = App\User::where(['username' => 'chef'])->first();
        
        Role::where('id', '!=', 0)->delete();
        Permission::where('id', '!=', 0)->delete();

        $adminRole = Role::create(['name' => 'Admin']);
        $managerRole = Role::create(['name' => 'Manager']);
        $staffRole = Role::create(['name' => 'Staff']);
        $chefRole = Role::create(['name' => 'Chef']);

        Permission::create(['name' => 'Manage users']);
        Permission::create(['name' => 'Manage branch users']);
        Permission::create(['name' => 'Manage branches']);
        Permission::create(['name' => 'Manage kitchens']);
        Permission::create(['name' => 'Manage branch kitchens']);
        Permission::create(['name' => 'Manage zones']);
        Permission::create(['name' => 'Manage branch zones']);
        Permission::create(['name' => 'Manage tables']);
        Permission::create(['name' => 'Manage branch tables']);
        Permission::create(['name' => 'Manage items']);
        Permission::create(['name' => 'Manage branch items']);
        Permission::create(['name' => 'Manage cuisine']);
        Permission::create(['name' => 'Manage branch cuisine']);
        Permission::create(['name' => 'Manage addons']);
        Permission::create(['name' => 'Manage branch addons']);
        Permission::create(['name' => 'Manage category']);
        Permission::create(['name' => 'Manage branch category']);
        Permission::create(['name' => 'Manage tax']);
        Permission::create(['name' => 'Manage billtitle']);
        Permission::create(['name' => 'Manage inventory']);
        Permission::create(['name' => 'Manage kot']);
        Permission::create(['name' => 'Manage branch kot']);
        $billingPermission = Permission::create(['name' => 'Manage billing']);
        
        $adminRole->givePermissionTo([
            'Manage users', 'Manage branches', 'Manage kitchens', 'Manage zones',
            'Manage tables', 'Manage items', 'Manage cuisine', 'Manage addons', 
            'Manage category', 'Manage tax', 'Manage billing', 'Manage inventory'
        ]);
        $managerRole->givePermissionTo([
            'Manage branch users', 'Manage branch kitchens', 'Manage branch zones',
            'Manage branch tables', 'Manage branch items', 'Manage branch cuisine', 'Manage branch addons', 
            'Manage branch category', 'Manage tax', 'Manage billing'
        ]);
        $staffRole->givePermissionTo([
            'Manage branch kitchens', 'Manage branch zones',
            'Manage branch tables', 'Manage branch items', 'Manage branch cuisine', 'Manage branch addons', 
            'Manage branch category', 'Manage tax', 'Manage billing'
        ]);
        $chefRole->givePermissionTo([
            'Manage branch kot'
        ]);
        
        $admin->assignRole('Admin');
        $manager->assignRole('Manager');
        $staff->assignRole('Staff');
        $chef->assignRole('Chef');
    }
}
