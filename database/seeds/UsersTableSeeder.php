<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Branch::class)->create();
        factory(App\User::class)->create(['username' => 'admin']);
        factory(App\User::class)->create(['username' => 'manager']);
        factory(App\User::class)->create(['username' => 'staff']);
        factory(App\User::class)->create(['username' => 'chef']);
    }
}
