<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitchenOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kitchen_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('kitchen_order_id');
            $table->foreign('kitchen_order_id')->references('id')->on('kitchen_orders')->onDelete('cascade');
            $table->unsignedInteger('kitchen_id')->nullable();
            $table->foreign('kitchen_id')->references('id')->on('kitchens')->onDelete('set null');
            $table->morphs('itemable');
            $table->string('name');
            $table->unsignedSmallInteger('quantity');
            $table->decimal('price', 7, 2);
            $table->decimal('cgst', 4, 2);
            $table->decimal('sgst', 4, 2);
            $table->decimal('total_amount', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kitchen_order_items');
    }
}
