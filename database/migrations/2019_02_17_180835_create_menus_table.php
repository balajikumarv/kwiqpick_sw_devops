<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('menu_type');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('SET NULL');
            $table->unsignedInteger('kitchen_id');
            $table->foreign('kitchen_id')->references('id')->on('kitchens')->onDelete('cascade');
            $table->string('menu_desc');
            $table->string('name');
            $table->string('plate', 2);
            $table->decimal('price', 7, 2);
            $table->decimal('half_plate', 7, 2);
            $table->decimal('familypack_plate', 7, 2);
            $table->string('menu_cuisine')->nullable();
            $table->string('menu_incl_excl');
            $table->string('menu_item_spicy_level');
            $table->string('menu_item_veg_nonveg');
            $table->string('menu_status');
            $table->string('short_code');
            $table->decimal('cgst', 4, 2);
            $table->decimal('sgst', 4, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
