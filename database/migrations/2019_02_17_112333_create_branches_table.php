<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch_id');
            $table->string('branch_name');
            $table->string('branch_number');
            $table->string('branch_google_map');
            $table->string('branch_address');
            $table->string('branch_country');
            $table->string('branch_state');
            $table->string('branch_city');
            $table->string('branch_image');
            $table->string('branch_manager_name');
            $table->string('branch_manager_number');
            $table->string('printer_name');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
