## Prerequisite packages

- [npm](https://nodejs.org/en/)
- [composer](https://getcomposer.org/)

To check whether above packages are installed, run below command

```shell
npm -v
composer -V
```

## Install Application

To get started with the app first make sure that the following changes are done

Copy `.env.example` file to `.env` and replace DB credentials.
Replace `APP_URL` with an application hostname.

```shell
npm install && npm run dev
composer install
php artisan migrate
php artisan db:seed
php artisan storage:link
```