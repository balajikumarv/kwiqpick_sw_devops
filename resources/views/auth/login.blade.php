@extends('layouts.auth-app')

@section('content')

<div class="wthree-form">
    <h2>Fill out the form below to login</h2>
    <div class="w3l-login form">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-sub-w3">
                <input id="username" type="text" class="{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus>
            </div>
            <div class="form-sub-w3">
                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
            </div>
            @if ($errors->has('username'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <div class="submit-agileits">
                <input type="submit" value="Login">
            </div>
                <!-- <a href="#">Forgot Password ?</a> -->
        </form>
    </div>
</div>
@endsection
