<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<script src="{{ asset('js/app.js') }}" defer></script>
<!--//Metis Menu -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
/* width */
::-webkit-scrollbar {
  width: 10px;
  border-radius:50px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
  border-radius:50px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
  border-radius:50px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555;
  border-radius:50px;
}
.table_radio_size{
	padding-right: 20px;
    padding-left: 20px;
}
#item_list_table{
	margin-bottom: 0px;
}
#item_st{
	font-size: 15px;
	left: 0px;
	padding-top: 2px;
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<div id="page-wrapper">
	<div class="main-page">
		<div class="forms">
			<div class="row" style="border-style: groove;">
				<div class="form-three widget-shadow" style="margin-top: 0px;padding: 12px;">
					<p class="statusMsg"></p>
					<div id="billing-order"></div>
					<form class="form-horizontal" enctype="multipart/form-data" id="insert_form" method="post" >
						<div class="form-group">
						<div class="col-md-2" style="border-right: 1px solid;border-color: #ddd;">
							<h4>Zone </h4>
							<div class="row">
								<select class="form-control" name="bill_zone" id="bill_zone" required="">
						    	<option hidden="">--- Select Zone ---</option>
						    	@if(count($zones) > 0)
	        							@foreach($zones->all() as $zone)
							    		<option value="{{ $zone->id }}">{{ $zone->name }}</option>
							    	@endforeach
			      				@else
			      				<option value="-"> -- No Data -- </option>
			      				@endif
						    </select>
						    <br>
								<div style="height: 415px;">
								  	<div id="c1" style="overflow-x: hidden; max-height: 415px;">
								  		<h4>Table </h4>
										<div class="form-group"id="bill_list_table">
											
												<!-- @if(count($tables) > 0)
				        							@foreach($tables->all() as $table)
											      	<div class="funkyradio-primary" style="padding-right: 20px;padding-left: 20px;">
											            <input type="radio" name="radio" id="table_{{ $table->id }}" value="{{ $table->id }}" />
											            <label for="table_{{ $table->id }}">{{ $table->name }}</label>
											        </div>
											        @endforeach
					      						@endif -->
											
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-2" style="border-right: 1px solid;border-color: #ddd;">
							<div class="row">
								<h4>Category </h4>
								<div style="height: 470px;">
									<div id="c1" style="overflow-x: hidden; max-height: 470px;">
										<div class="form-group">
											<div class="funkyradio">
												@if(count($categories) > 0)
				        							@foreach($categories->all() as $category)
											     	<div class="funkyradio-primary" style="padding-right: 20px;padding-left: 20px;">
											            <input type="radio" name="category_selected" class="category_selected" id="{{ $category->id }}" value="{{ $category->id }}" />
											            <label for="{{ $category->id }}">{{ $category->category }}</label>
											        </div>
											        @endforeach
					      						@endif
										    </div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4" style="border-right: 1px solid;border-color: #ddd;width: 30%;">
							<h4>Items </h4>
						    <br>
							<div class="row">
								<div style="height: 270px;">
									<div id="c1" class="itam_list_checkbox" style="overflow-x: hidden; max-height: 270px;">
										<!-- @if(count($items) > 0)
		        							@foreach($items->all() as $item)
		        							<div class="col-md-6">
			        							<div class="form-group" id="item_list_table">
													<label class="container_c" id="item_st">{{ $item->name }}
													  <input type="checkbox" value="{{ $item->id }}">
													  <span class="checkmark_c"></span>
													</label>
												</div>
											</div>
											@endforeach
			      						@endif -->
											
										</div>
										<br>
										<div class="row">
											<h4>Add-ons </h4>
											<hr>
											<div style="height: 100px;">
												<div id="c1" style="overflow-x: hidden; max-height: 100px;" class="addon_list">
												
							      				</div>
							      			</div>
										</div>
									</div>
								</div>
								
								
						</div>
						<div class="col-md-4" style="width: 36%;">
							<div class="row">
								<div class="col-sm-8" style="padding-left: 0px;">
									<input type="text" class="form-control1" ng-model="firstName" id="emp_first_name" name="emp_first_name" placeholder="Enter Menu Name Or Number" required="">		
								</div>
								<div class="col-sm-4">
									<input type="submit" value="ADD" class="btn btn-success">
								</div>
							</div>
							<table class="table table-bordered"  id="item_in_table" style="margin-bottom:0px;">
							    
							      <tr>
							        <th>Product</th>
							        <th>Rate</th>
							        <th>Quantity</th>
							        <th>Amount</th>
							        <th><i class="fa fa-trash" aria-hidden="true"></i></th>
							      </tr>
							    
							  </table>
							  <br>
							  <div class="row">
							  	<div class="col-md-4">
							  		<p>Sub Total</p>
							  	</div>
							  	<div class="col-md-8">
							  		<input type="number" id="sub_total_value" class="form-control" name="" value="0.00">
							  	</div>
							  	<div class="col-md-4">
							  		<p>VAT%</p>
							  	</div>
							  	<div class="col-md-8">
							  		<input type="number" id="vat_total_value" class="form-control" name="" value="20.0">
							  	</div>
							  	<div class="col-md-4">
							  		<p>Grand Total</p>
							  	</div>
							  	<div class="col-md-8">
							  		<input type="number" id="grand_total_value" class="form-control" name="" value="0.00">
							  	</div>
							  </div>
							  <div class="row">
							  	<button type="button" class="btn btn-primary">KOT</button>
							  	<a href="api/generate-pdf" target="_blank" id="printPage" class="btn btn-warning">Print</a>
							  	<button type="button" class="btn btn-success">Sattlement</button>
							  	<button type="button" class="btn btn-danger">Clear</button>
							  </div>
							  <!-- <div class="bg-dark light pv20 text-white fw600 text-center">Total - 800</div>
							<div class="bg-dark pv20 text-white fw600 text-center">Print / Scan</div> -->
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--footer-->
	@include('inc.footer')
        <!--//footer-->
</div>
<script type="text/javascript">
$(document).ready(function(e){
	/*Start zone to table*/
	var sub_tot = 0;
	var grand_tot = 0;
	$('#bill_zone').change(function(){
    var bill_zoneID = $(this).val();    
    if(bill_zoneID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-table-list')}}?bill_zone_id="+bill_zoneID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
                $("#bill_list_table").empty();
                $.each(res,function(key,value){
                	$("#bill_list_table").append('<div class="funkyradio table_radio_size"><input type="radio" name="radio" id="table_'+value['id']+'" value="'+value['id']+'"><label for="table_'+value['id']+'">'+value['name']+'</label></div>');
                });
           
            }else{
               $("#bill_list_table").empty();
            }
           }
        });
    }else{
        $("#bill_list_table").empty();
    }      
   });
	/*// end zone to  table*/

	/* Billing add to cart */
	
	$(".itam_list_checkbox").on('click', '.item_selected', function (event) {
		var el = $(event.target);
		var id = el.data('id');
		if(event.target.checked) {
			var name = el.data('name');
			var amount = el.data('price');
			addCartItem(id, name, amount, 'menu')
		}else {
			removeCartItem(id, 'menu')
		}
		getAddons();
	})
	$(".addon_list").on('click', '.addon_item_input', function (event) {
		var el = $(event.target);
		var id = el.data('id');
		if(event.target.checked) {
			var name = el.data('name');
			var amount = el.data('price');
			addCartItem(id, name, amount, 'addon')
		}else{
			removeCartItem(id, 'addon')
		}
	})
	bindCartItemEvents();
	function addCartItem(id, name, amount, type) {
		$("#item_in_table tr:last").after('<tr data-id="'+id+'" data-type="'+type+'"><td>'+name+'</td><td>'+amount+'</td><td><input data-price="'+amount+'" data-id="'+id+'" type="number" class="form-control quantity" name="" value="1"></td><td><span id="'+id+'" class="list_of_amount">'+amount+'</span></td><th><i class="fa fa-trash remove-item" aria-hidden="true"></i></th></tr>');
		calcPrices();
	}
	function bindCartItemEvents() {
		$('#item_in_table').on('change', '.quantity', function(event){
			calcPrices();
		})
		$('#item_in_table').on('keyup', '.quantity', function(event){
			calcPrices();
		})
		$('#item_in_table').on('click', '.remove-item', function(event){
			var item_row = event.target.closest('tr');
			removeCartItem(item_row.getAttribute('data-id'), item_row.getAttribute('data-type'))
		})
	}
	function removeCartItem(id, type){
		var item_row = $('#item_in_table tr[data-id="'+id+'"][data-type="'+type+'"]');
		if(item_row.length == 0) return;
		item_row = item_row[0]
		var id = item_row.getAttribute('data-id')
		var type = item_row.getAttribute('data-type')
		if(type == 'addon'){
			$('.addon_list .item_selected[data-id="'+id+'"]')[0].checked = false
		}else{
			$('.itam_list_checkbox .item_selected[data-id="'+id+'"]')[0].checked = false
		}
		item_row.parentNode.removeChild(item_row)
		calcPrices();
	}
	function calcPrices() {
		var tot_quantity = 0;
		var tot_price = 0;
		$('#item_in_table .quantity').each(function(index, el){
			var price =  el.getAttribute('data-price')
			tot_price += parseFloat(el.value) * parseFloat(price);
		})
		$('#sub_total_value').val(tot_price.toFixed(2));
		var current_vat_value = $('#vat_total_value').val();
		grand_tot = parseFloat(tot_price) * parseFloat(current_vat_value)/100;
		$('#grand_total_value').val(grand_tot.toFixed(2));
	}
	function getAddons(params) {
		var ids = []
		$('.itam_list_checkbox .item_selected:checked').each(function(i, el){
			ids.push(el.value)
		});
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type:"GET",
			url:"{{url('api/get-item-selected')}}",
			data: {
				item_ids: ids
			},
			success:function(addons){
				$('.addon_list .addon-item').addClass('remove')
				$.each(addons,function(key,addon){
					var el = $('.addon_list .addon-item[data-id="'+addon['id']+'"]');
					if(el.length) {
						el.removeClass('remove')
					}else{
						$(".addon_list").append('<div class="col-md-6 addon-item" data-id="'+addon['id']+'">'+
						'<div class="form-group" id="item_list_table"><label class="container_c" id="item_st">'+
						addon['addon']+'<input type="checkbox" value="'+addon['id']+'" class="item_selected addon_item_input" data-id="'+addon['id']+'" data-name="'+addon['addon']+'" data-price="'+addon['price']+'" name="addon_'+addon['id']+'">'+
						'<span class="checkmark_c"></span></label></div></div>');
					}
				});
				$('.addon_list .addon-item.remove').each(function(i, el){
					removeCartItem(el.getAttribute('data-id'), 'addon')
				})
				$('.addon_list .addon-item.remove').remove()
			}
		});
	}
	/*---------------------------------------------------------------------------------------------------*/
	/*start categories to items*/
	$('.category_selected').on('click',function(){
    /*var category_ID  = $('input[name=category_selected]:checked', '#insert_form').val();*/    
    var category_ID  = $(this).val(); 

    if(category_ID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-item-list')}}?category_id="+category_ID,
           success:function(res){               
            if(res){
				$(".itam_list_checkbox").empty();
                $.each(res,function(key,value){
                    $(".itam_list_checkbox").append('<div class="col-md-6"><div class="form-group" id="item_list_table"><label class="container_c" id="item_st">'+value['name']+'<input type="checkbox" value="'+value['id']+'" class="item_selected" data-id="'+value['id']+'" data-name="'+value['name']+'" data-price="'+value['menu_price']+'"id="item_'+value['id']+'" name="'+value['id']+'"><span class="checkmark_c"></span></label></div></div>');
				});           
            }else{
               $(".itam_list_checkbox").empty();
            }
           }

        });
    }else{
        $(".itam_list_checkbox").empty();
    }

    });
	/*// end categories to items*/
	/*------------------------------------------------------------------------------------------------------*/

});
</script>

<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>

</body>
</html>

