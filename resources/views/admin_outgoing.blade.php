<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>

<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
	.border_table{
		border: solid 1px;
		border-color: #716d6d;
		margin-top: 0px;
		padding: 9px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<!-- <h4>Outgoing Details<label  class="pull-right" data-toggle="modal" data-target="#add_outgoing">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4> -->
				<h4>Outgoing Details<a href="{{ url('add_outgoing') }}"><label  class="pull-right">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label></a></h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Record No.</th>
							<th>Branch</th>
							<th>Created On</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($outgoings) > 0)
        					@foreach($outgoings->all() as $outgoing)
						<tr>
							<th></th>
							<th scope="row">
								{{ $outgoing->record_number }}
							</th>
							<td>{{ $outgoing->branch->branch_name }}</td>
							<td>{{ $outgoing->created_at }}</td>
							<td>
								<a href='{{ url("view_outgoing/{$outgoing->record_number}") }}'><span data-toggle="tooltip" title="View" class="cursor_point"><i class="fa fa-folder-open-o" aria-hidden="true"></i></span></a>
							</td>
						</tr>
						
							@endforeach
	      				@endif
					</tbody>
				</table>
					<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>

@include('inc.footer')
</div>
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "outgoing/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});   

});


</script>
<!-- password Validation -->
<script type="text/javascript">
	var password = document.getElementById("staff_password")
  , confirm_password = document.getElementById("c_staff_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
<!-- //end validation for password -->
<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Outgoing Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Outgoing Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Outgoing Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>

