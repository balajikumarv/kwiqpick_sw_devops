<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>

<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
	.border_table{
		border: solid 1px;
		border-color: #716d6d;
		margin-top: 0px;
		padding: 9px;
	}
.glyphicon-minus{
	color: #fff;
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<div id="page-wrapper">
	<div class="main-page">
		<div class="forms">
			<div class="row">
				<div class="form-three widget-shadow">
					<p class="statusMsg"></p>
					<form class="form-horizontal" enctype="multipart/form-data" id="insert_form" method="post">
						<h4><a href="{{ url('admin_outgoing') }}">All OutGoing</a></h4>
						<br>
						<div class="form-group">
							<label for="Occupation" class="col-sm-2 control-label">Record No.</label>
							<div class="col-sm-4">
								<input type="text" name="record_no" id="record_no" class="form-control1" required="" placeholder="Enter Record No.">
							</div>
							<label for="Occupation" class="col-sm-1 control-label">Branch</label>
							<div class="col-sm-4">
								<select class="form-control1" name="branch_id" id="branch_id">
									<option value="0"> Select Branch</option>
							    	@if(count($branches) > 0)
										@foreach($branches->all() as $branch)
											<option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
										@endforeach
									@else
										<option value="-"> -- No Data -- </option>
									@endif
							    </select>
							</div>
							<div class="col-sm-2">
							</div>
						</div>
						<h4>Add Ingredient</h4>
						<br>
						<span id="result"></span>
						<table class="table table-bordered" id="item_table">
							<tr>
								<th width="30%">Category</th>
								<th width="30%">Ingredient</th>
								<th width="20%">Unit</th>
								<th width="20%">Quantity</th>
								<th>
									<i class="fa fa-cogs" aria-hidden="true"></i>
								</th>
							</tr>
							<tr>
								<td>
									<select class="form-control category" name="category_id[]" id="category_1">
										<option value="0">Select Category</option>
										@if(count($ingredient_categories) > 0)
											@foreach($ingredient_categories->all() as $ingredient_category)
												<option value="{{ $ingredient_category->id }}">{{ $ingredient_category->category }}</option>
											@endforeach
										@else
											<option value="-"> -- No Data -- </option>
										@endif
									</select>
								</td>
								<td>
									<select name="ingredient_id[]" id="ingredient_1" class="form-control ingredient" required="">
								    	<option value="-"> -- No Data -- </option>
			                		</select>
								</td>
								<td>
									<select name="unit_id[]" id="unit_1" class="form-control" required="">
								    	<option value="-"> -- No Data -- </option>
			                		</select>
								</td>
								<td>
									<input type="text" name="quantity[]" class="form-control income_quantity" id="income_quantity_1" placeholder="0" />
									<input type="hidden" name="row_count" id="row_count" value="1">
								</td>
								<td>
									<button type="button" name="add" class="btn btn-success btn-sm add">
										<span class="glyphicon glyphicon-plus" style="color: #fff;"></span>
									</button>
								</td>
							</tr>
						</table>
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label"></label>
							<div class="col-sm-8">
								<input type="submit" name="submit" class="btn btn-success submitBtn" value="SAVE"/>
								<a href="{{ url('admin_outgoing') }}" class="btn btn-danger">GO BACK</a>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@include('inc.footer')
</div>
<!-- script start -->
<script>
$(document).ready(function(){
 
 $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }); 

$(document).on('click', '.add', function(){
  var html = '';
  var row_count = $('#row_count').val();
  var row_count_val = +row_count+ 1;
  /*alert(row_count_val);*/
  $("#row_count").val(row_count_val);
  html += '<tr>';
  html += '<td>'+
  			'<select class="form-control category" name="category_id[]" id="category_'+row_count_val+'">'+
  				'<option value="0"> Select Category </option>'+
				'@if(count($ingredient_categories) > 0)'+
					'@foreach($ingredient_categories->all() as $ingredient_category)'+
						'<option value="{{ $ingredient_category->id }}">{{ $ingredient_category->category }}</option>'+
					'@endforeach'+
				'@else'+
					'<option value="-"> -- No Data -- </option>'+
				'@endif'+
			'</select>'+
			'</td>';
  html += '<td>'+
  			'<select name="ingredient_id[]" id="ingredient_'+row_count_val+'" class="form-control ingredient" required="">'+
		    	'<option value="-"> -- No Data -- </option>'+
    		'</select>';
  html += '<td>'+
  			'<select name="unit_id[]" id="unit_'+row_count_val+'" class="form-control" required="">'+
		    	'<option value="-"> -- No Data -- </option>'+
    		'</select>';
  html += '<td><input type="text" name="quantity[]" id="quantity_'+row_count_val+'" class="form-control income_quality" placeholder="0" /></td>';
   html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
  $('#item_table').append(html);
 });
 
 $(document).on('click', '.remove', function(){
  $(this).closest('tr').remove();
 });

  /*dropdown*/
 $(document).on('change', '.category', function(){
 	var el = this;
	var id = this.id;
	var splitid = id.split("_");
	var tr_row = splitid[1];
	/*alert(tr_row);*/
    var categoryID = $(this).val();    
    if(categoryID){
    	$.ajax({
           type:"GET",
           url:"{{url('api/get-ingredient-list')}}?category_id="+categoryID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
            	$('#ingredient_'+tr_row).empty();
            	$('#ingredient_'+tr_row).append('<option value="0">Select </option>');
                /*$(this).closest('tr').children('td').find('#ingredient').empty();
                $(this).closest('tr').children('td').find('#ingredient').append('<option>Select</option>');*/
                $.each(res,function(key,value){
                	$("#ingredient_"+tr_row).append('<option value="'+value['id']+'">'+value['ingredient']+'</option>');
                });
           
            }else{
               $("#ingredient_"+tr_row).empty();
            }
           }
        });
    }else{
        $("#ingredient_"+tr_row).empty();
    }      
   });
	 $(document).on('change', '.ingredient', function(){
	 	var el = this;
		var id = this.id;
		var splitid = id.split("_");
		var tr_row = splitid[1];

	    var ingredientID = $(this).val();    
	    if(ingredientID){
	    	$.ajax({
	           type:"GET",
	           url:"{{url('api/get-unit-list')}}?ingredient_id="+ingredientID,
	           success:function(res){    
	            if(res){
	            	/*console.log(res);*/
	                $("#unit_"+tr_row).empty();
	                /*$("#unit").append('<option>Select</option>');*/
	                $.each(res,function(key,value){
	                	$("#unit_"+tr_row).append('<option value="'+value['id']+'">'+value['unit']+'</option>');
	                });
	           
	            }else{
	               $("#unit_"+tr_row).empty();
	            }
	           }
	        });
	    }else{
	        $("#unit_"+tr_row).empty();
	    }      
	   });
 
 $('#insert_form').on('submit', function(event){
  event.preventDefault();
  $.ajax({
    url:"{{ url('create_outgoing') }}",
    method:"POST",
    data:$(this).serialize(),
    dataType:'json',
    beforeSend:function(){
    	$('#save').attr('disabled', 'disabled');
    },
    success:function(data)
    {
     if(data.error)
     {
      var error_html= '';
      for(var count = 0; count < data.error.length; count++){
      	error_html += '<p>'+data.error[count]+'</p>';
      }
      $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
     }
     else{
     	alert(data.success);
     	$('#insert_form').trigger("reset");
    	/*console.log(data.success);
    	$('result').html('<div class="alert alert-success">'+data.success+'</div>');*/
     }
     $('#save').attr('disabled', false);
    }
   });
  
 }); 
});
</script>
<!-- // end script -->
<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>

	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>

