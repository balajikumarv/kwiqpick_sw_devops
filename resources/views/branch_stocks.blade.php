<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf_token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="{{asset('css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('data_tables/css/jquery.dataTables.css')}}" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="{{asset('css/style.css')}}" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="{{asset('css/font-awesome.css')}}" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="{{asset('css/SidebarNav.min.css')}}" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('js/modernizr.custom.js')}}"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="{{asset('js/Chart.js')}}"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="{{asset('js/metisMenu.min.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<link href="{{asset('css/custom.css')}}" rel="stylesheet">
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert.css')}}">
<script type="text/javascript" src="{{asset('js/sweetalert.js')}}"></script>

<!-- //SweetAlert -->
<!--//Metis Menu -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
.modal-content{
	border-radius: 0px;
}
.heading{
	margin-bottom: 2px;
}

</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>Stocks Details 
					<div class="dropdown pull-right">
				    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Branch Stock
				    <span class="caret"></span></button>
				    <ul class="dropdown-menu">
				      @if(count($branches) > 0)
	        					@foreach($branches->all() as $branch)
						    		<li><a href='{{ url("stock/branch/stock/{$branch->id}") }}'>{{ $branch->branch_name }}</a></li>
						    	@endforeach
		      				@else
		      				<li><a href="#">No Branch Available</a></li>
		      			@endif
				    </ul>
				</div>
				</h4>
				<div class="loading-overlay"><div class="overlay-content">Loading.....</div></div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Ingredient</th>
							<th>Quantity</th>
							<th>Created</th>
						</tr>
					</thead>
					<tbody>
						@if(count($stocks) > 0)
        					@foreach($stocks->all() as $stock)
						<tr>
							<th></th>
							<th scope="row">{{ $stock->ingredient->ingredient }}</th>
							<td>{{ $stock->quantity }}</td>
							<td>{{ $stock->created_at->format('d/m/Y') }}</td>
						</tr>
							@endforeach
	      				@endif
					</tbody>
				</table>
			<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- Form Start -->
<!-- Form End -->
<!-- Footer Start -->
@include('inc.footer')
<!-- Footer End -->
</div>
<script type="text/javascript">
// Show loading overlay when ajax request starts
$( document ).ajaxStart(function() {
    $('.loading-overlay').show();
});
// Hide loading overlay when ajax request completes
$( document ).ajaxStop(function() {
    $('.loading-overlay').hide();
});
</script>
	<!-- side nav js -->
	<script src="{{asset('js/SidebarNav.min.js')}}" type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="{{asset('js/classie.js')}}"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="{{asset('js/jquery.nicescroll.js')}}"></script>
	<script src="{{asset('js/scripts.js')}}"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="{{asset('js/bootstrap.js')}}"> </script>
	<!-- Data Tables -->
	<script src="{{asset('data_tables/js/jquery.dataTables.js')}}"></script>
	<script src="{{asset('data_tables/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('data_tables/js/jszip.min.js')}}"></script>
	<script src="{{asset('data_tables/js/pdfmake.min.js')}}"></script>
	<script src="{{asset('data_tables/js/vfs_fonts.js')}}"></script>
	<script src="{{asset('data_tables/js/buttons.html5.min.js')}}"></script>
	<script src="{{asset('data_tables/js/buttons.print.min.js')}}"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branch Stock Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branch Stock Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branch Stock Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>