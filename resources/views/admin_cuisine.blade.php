<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>
<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			
			<div class="table-responsive bs-example widget-shadow">
				<h4>Cuisine Details<label  class="pull-right" data-toggle="modal" data-target="#add_cuisine">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Cuisine</th>
							<th>Created</th>
							<th>Status</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($cuisines) > 0)
        					@foreach($cuisines->all() as $cuisine)
						<tr>
							<th></th>
							<th scope="row">{{ $cuisine->cuisine }}</th>
							<td>{{ $cuisine->created_at->format('d/m/Y') }}</td>
							<td>
								@if($cuisine->status == 1)
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								@else
								<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								@endif
							</td>
							
							<td>
							<label data-toggle="modal" data-target="#edit_cuisine{{ $cuisine->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
							<label class="deleteData cursor_point" data-id="{{ $cuisine->id }}" id="{{ $cuisine->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>
						<!-- Edit tax  -->
						  <div class="modal fade" id="edit_cuisine{{ $cuisine->id }}" role="dialog">
						    <div class="modal-dialog">
						    <!-- Modal content-->
						      <div class="modal-content">
						      	<form  class="update_form" id="updateForm{{ $cuisine->id }}" data-toogle="validator">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Edit {{ $cuisine->cuisine }} Cuisine</h4>
						        </div>
						        <div class="modal-body">
									<div class="form-group">
										<label for="ID">Cuisine Name</label>
										<input type="text" class="form-control" placeholder="Enter Cuisine Name" id="edit_cuisine" name="edit_cuisine" value="{{ $cuisine->cuisine }}">
										<input type="hidden" class="form-control" name="cuisine_id" id="cuisine_id" value="{{ $cuisine->id }}">
									</div>  			
									<div class="form-group">
										<label class="container_radio">Active
										  <input type="radio" name="edit_status" value="1" {{ ($cuisine->status=="1")? "checked" : "" }}>
										  <span class="checkmark_radio"></span>
										</label>
										<label class="container_radio">Inactive
										  <input type="radio" value="0" name="edit_status" {{ ($cuisine->status=="0")? "checked" : "" }}>
										  <span class="checkmark_radio"></span>
										</label>
									</div>  			
									</div>
						        <div class="modal-footer">
						          <input type="hidden" value="{{$cuisine->id}}" class="cuisine_id"/>
								    <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						        </div>
						        </form>
						      </div>
						    </div>
						  </div>
						<!-- // End Tax -->
							@endforeach
	      				@endif
					</tbody>
				</table>
			<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>

<!-- Add tax  -->
  <div class="modal fade" id="add_cuisine" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
      	<form method="post" id="insert_form" enctype="multipart/form-data" data-toogle="validator">
          @csrf {{ method_field('POST') }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Cuisine</h4>
        </div>
        <div class="modal-body">
			<div class="form-group">
				<label for="ID">Cuisine Name</label>
				<input type="text" class="form-control" required="" placeholder="Enter Cuisine Name" id="cuisine_name" name="cuisine_name">
			</div>
			<div class="form-group">
				<label class="container_radio">Active
				  <input type="radio" name="cuisine_status" value="1" checked="">
				  <span class="checkmark_radio"></span>
				</label>
				<label class="container_radio">Inactive
				  <input type="radio" value="0" name="cuisine_status">
				  <span class="checkmark_radio"></span>
				</label>
			</div> 
			</div>
        <div class="modal-footer">
          <input type="submit" name="submit" class="btn btn-primary submitBtn" id="insertbutton" value="Save">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- // End tax -->


<!-- main content End-->
@include('inc.footer')
</div>
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
    $("#insert_form").on('submit', function(e){
      e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "{{ url('create_cuisine') }}",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              $('#add_cuisine').modal('hide');
              $('#insert_form')[0].reset();
              swal("Good job!", "New Cuisine Details Created Successfully!", "success");
              window.location.href= 'admin_cuisine';
			},
            error : function(data){
              $('#add_cuisine').modal('hide');
              swal({
                title: 'Oops...',
                text: data.message,
                type: 'error',
                timer: '1500' 
              })
            }
        });
        return false;
    });

    //Edit
    $(".updateBtn").on('click', function(e){
      var id = $(this).parent().find('.cuisine_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "cuisine/edit",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_cuisine'+id).modal('hide');
              $('.update_form')[0].reset();
              window.location.href= 'admin_cuisine';
              swal("Good job!", "Cuisine Details Updated Successfully!", "success");
            },
            error : function(response){
              $('#edit_cuisine'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });

    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "cuisine/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});   

});
</script>
<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Cuisine Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Cuisine Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Cuisine Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>


