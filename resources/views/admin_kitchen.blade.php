<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>

<!-- //SweetAlert -->
<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
.modal-content{
	border-radius: 0px;
}
.heading{
	margin-bottom: 2px;
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>Kitchen Details<label  class="pull-right" data-toggle="modal" data-target="#add_kitchen">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Name</th>
							<th>Branch</th>
							<th>Created</th>
							<th>Printer</th>
							<th>Status</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($kitchens) > 0)
        					@foreach($kitchens->all() as $kitchen)
						<tr>
							<th></th>
							<th scope="row">{{ $kitchen->name }}</th>
							<td>{{ $kitchen->branch->branch_name }}</td>
							<td>{{ $kitchen->created_at->format('d/m/Y') }}</td>
							<td>
								@if ($kitchen->printer_name)
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								@else
									<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								@endif
							</td>
							<td>
								@if ($kitchen->status == 1)
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								@else
									<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								@endif
							</td>
							<td>
							<label data-toggle="modal" data-target="#edit_kitchen{{ $kitchen->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
							<label class="deleteData cursor_point" data-id="{{ $kitchen->id }}" id="{{ $kitchen->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>
						<!-- Edit Kitchen  -->
						  <div class="modal fade" id="edit_kitchen{{ $kitchen->id }}" role="dialog">
						    <div class="modal-dialog">
						    <!-- Modal content-->
						      <div class="modal-content">
						      	<form  class="update_form" id="updateForm{{ $kitchen->id }}" data-toogle="validator">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Edit {{ $kitchen->name }} Details</h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row">
						        		<div class="form-group">
											<label for="ID">Kitchen Name</label>
											<input type="text" class="form-control" placeholder="Enter Kitchen Name" id="edit_kitchen_name" value="{{ $kitchen->name }}" name="edit_kitchen_name">
											<input type="hidden" class="form-control" name="kitchen_id" id="kitchen_id" value="{{ $kitchen->id }}">
										</div>	
						        	</div>
						        	<div class="row">
						        		<div class="form-group">
										    <label for="Number">Branch</label>
										    <select class="form-control" name="edit_kitchen_branch" id="edit_kitchen_branch">
										    	@if(count($branches) > 0)
													@foreach($branches->all() as $branch)
														<option value="{{ $branch->id }}" {{$kitchen->branch_id == "$branch->id"  ? 'selected' : ''}}>{{ $branch->branch_name }}</option>
													@endforeach
												@else
													<option value="-"> -- No Data -- </option>
												@endif
											</select>
										</div>	
						        	</div>
						        	<div class="row" id="chef_edit_list">
						        		<div class="form-group">
												<label for="Number">Chef's</label>
													<div style="height: 114px;">
														<div id="c1" style="overflow: auto; max-height: 100px;">
														<!-- addon list -->
														
														<?php 
																$chef_list = $kitchen->chef;
																$kitchen_chef = explode(',', $chef_list);
																
																    ?>

														@if(count($users) > 0)
															@foreach($users->all() as $user)
																<label class="container_checkbox">{{ $user->name }}
																<input type="checkbox" name="edit_chef[]" value="{{ $user->id }}" {{in_array($user->id,$kitchen_chef)?'checked':''}} >
																<span class="checkmark_checkbox"></span>
																</label>
															@endforeach
														@else
																<span>No Data In Addons</span>
														@endif

														

														<!-- end -->
														</div>
												</div>
											</div>
						        	</div>
						        	<div class="row">
						        		<div id="edit_chef_list">
						        			
						        		</div>
						        	</div>
									<div class="row">
				
										<label for="Manager"></label>
										<div class="form-group">
											<label class="container_radio">Printer
											  <input type="radio" name="edit_printer_st" value="1">
											  <span class="checkmark_radio"></span>
											</label>
											<label class="container_radio">No Printer
											  <input type="radio" name="edit_printer_st" value="0" checked="">
											  <span class="checkmark_radio"></span>
											</label>
										</div>	
										
									</div>
									<div class="row edit_printer_div" style="display: none;">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Enter Printer Name" id="edit_printer_name" name="edit_printer_name">
										</div>	
									</div>	
									<div class="row">
										<div class="form-group">
											<label class="container_radio">Active
											  <input type="radio" name="edit_kitchen_status" value="1" {{ ($kitchen->status=="1")? "checked" : "" }}>
											  <span class="checkmark_radio"></span>
											</label>
											<label class="container_radio">Inactive
											  <input type="radio" value="0" name="edit_kitchen_status" {{ ($kitchen->status=="0")? "checked" : "" }}>
											  <span class="checkmark_radio"></span>
											</label>
										</div>
									</div>
									
									</div>
						        <div class="modal-footer">
						         <input type="hidden" value="{{$kitchen->id}}" class="kitchen_id"/>
								 <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								 <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						        </div>
						        </form>
						      </div>
						    </div>
						  </div>
						<!-- // End Kitchen -->
							@endforeach
	      				@endif
					</tbody>
				</table>
			<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- Add Kitchen  -->
  <div class="modal fade" id="add_kitchen" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
      	<form method="post" id="insert_form" enctype="multipart/form-data" data-toogle="validator">
          @csrf {{ method_field('POST') }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Kitchen</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="form-group">
					<label for="ID">Kitchen Name</label>
					<input type="text" class="form-control" placeholder="Enter Kitchen Name" id="kitchen_name" name="kitchen_name" required="">
				</div>	
        	</div>
        	<div class="row">
        		<div class="form-group">
				    <label for="Number">Branch</label>
				    <select class="form-control" name="kitchen_branch" id="kitchen_branch" required="">
				    	<option hidden="">--- Select Branch ---</option>
					    	@if(count($branches) > 0)
	        					@foreach($branches->all() as $branch)
						    		<option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
						    	@endforeach
		      				@else
		      				<option value="-"> -- No Data -- </option>
		      				@endif
					</select>
				</div>	
        	</div>
        	<div class="row">
        		<div id="chef_list">
        			
        		</div>
        	</div>
			<div class="row">
				
				<label for="Manager"></label>
				<div class="form-group">
					<label class="container_radio">Printer
					  <input type="radio" name="printer_st" value="1">
					  <span class="checkmark_radio"></span>
					</label>
					<label class="container_radio">No Printer
					  <input type="radio" name="printer_st" value="0" checked="">
					  <span class="checkmark_radio"></span>
					</label>
				</div>	
				
			</div>
			<div class="row printer_div" style="display: none;">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Enter Printer Name" id="printer_name" value="" name="printer_name">
				</div>	
			</div>	
			<div class="row">
				<div class="form-group">
					<label class="container_radio">Active
					  <input type="radio" name="kitchen_status" value="1" checked="">
					  <span class="checkmark_radio"></span>
					</label>
					<label class="container_radio">Inactive
					  <input type="radio" name="kitchen_status" value="0">
					  <span class="checkmark_radio"></span>
					</label>
				</div>	
			</div>		
			
			
			</div>
        <div class="modal-footer">
          <input type="submit" name="submit" class="btn btn-primary submitBtn" id="insertbutton" value="Save">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- // End Kitchen -->


<!-- main content End-->
@include('inc.footer')
</div>
<script type="text/javascript">
	$(document).ready(function() {
	    $("input[name$='printer_st']").click(function() {
	        var printer_s = $(this).val();

	        if (printer_s == 1) {
	        	$(".printer_div").show();
	        }else{
	        	$(".printer_div").hide();
	        }
	       
	    });
	    $("input[name$='edit_printer_st']").click(function() {
	        var printer_s = $(this).val();

	        if (printer_s == 1) {
	        	$(".edit_printer_div").show();
	        }else{
	        	$(".edit_printer_div").hide();
	        }
	       
	    });
	});
</script>
<script type="text/javascript">
function valueChanged()
{
    if($('.printer_ac').is(":checked"))   
        $(".printer_div").show();
    else
        $(".printer_div").hide();
}
</script>
<script type="text/javascript">
  /*main*/

  $(document).ready(function(e){
    $("#insert_form").on('submit', function(e){
      e.preventDefault();
      	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
            type: 'POST',
            url: "{{ url('create_kitchen') }}",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              $('#add_kitchen').modal('hide');
              $('#insert_form')[0].reset();
              /*window.location.href= 'admin_kitchen';*/
              setTimeout(function () {
                        window.location.href= 'admin_kitchen'; // the redirect goes here

                    },1000);
              swal("Good job!", "New Kitchen Created Successfully!", "success");

            },
            error : function(data){
              $('#add_kitchen').modal('hide');
              swal({
                title: 'Oops...',
                text: data.message,
                type: 'error',
                timer: '1500' 
              })
            }
        });
        return false;
    });

    //Edit
    $(".updateBtn").on('click', function(e){
      var id = $(this).parent().find('.kitchen_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "kitchen/edit",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_kitchen'+id).modal('hide');
              $('.update_form')[0].reset();
              window.location.href= 'admin_kitchen';
              swal("Good job!", "Kitchen Details Updated Successfully!", "success");
            },
            error : function(response){
              $('#edit_kitchen'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });

    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes Delete",
		  cancelButtonText: "No CANCEL",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "kitchen/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Selected file has been deleted", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	}); 

	/*dropdown*/
 $('#kitchen_branch').change(function(){
    var branchID = $(this).val();    
    if(branchID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-chef-list')}}?branch_id="+branchID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
                $("#chef_list").empty();
                $("#chef_list").append('Please Select Chef');
                $.each(res,function(key,value){
                	$("#chef_list").append('<label class="container_checkbox">'+value['name']+'														<input type="checkbox" name="chef[]" value="'+value['id']+'">																<span class="checkmark_checkbox"></span>																</label>');
                });
           
            }else{
               $("#chef_list").empty();
            }
           }
        });
    }else{
        $("#chef_list").empty();
    }      
   }); 

   $('#edit_kitchen_branch').change(function(){
    var branchID = $(this).val();    
    if(branchID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-chef-list')}}?branch_id="+branchID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
            	
                $("#edit_chef_list").empty();
                $("#edit_chef_list").append('Please Select Chef');
                $.each(res,function(key,value){
                	$("#edit_chef_list").append('<label class="container_checkbox">'+value['name']+'														<input type="checkbox" name="chef[]" value="'+value['id']+'">																<span class="checkmark_checkbox"></span>																</label>');
                });
           
            }else{
               $("#edit_chef_list").empty();
            }
           }
        });
    }else{
        $("#edit_chef_list").empty();
    }      
   });  

});


</script>
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Kitchen Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Kitchen Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Kitchen Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>