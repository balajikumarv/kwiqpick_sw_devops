<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE html>
<html>
<head>
<title>RESTAURANT | LogIn</title>
<!-- metatags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="food court login form a Flat Responsive Widget,Login form widgets, Sign up Web 	forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<link href="css/login_css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--online fonts-->
<link href="//fonts.googleapis.com/css?family=Lobster&amp;subset=cyrillic,latin-ext,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Righteous&amp;subset=latin-ext" rel="stylesheet">
<!--//online fonts-->
</head>
<body>
	<!-- <h1>f<span>oo</span>d c<span>o</span>urt l<span>o</span>g<span>in</span> f<span>o</span>rm</h1> -->
	<h1>RESTAURANT | LogIn</h1>
	<div class="wthree-form">
			<h2>Fill out the form below to login</h2>
		<div class="w3l-login form">
			<form action="#" method="post">
				<div class="form-sub-w3">
					<input type="text" name="Username" placeholder="Username" required=""/>
				</div>
				<div class="form-sub-w3">
					<input type="password" name="Password" placeholder="Password" required=""/>
				</div>
				<!-- <label class="anim">
					<input type="checkbox" class="checkbox">
					<span>Remember Me</span> 
				</label> -->
				<div class="submit-agileits">
					<input type="submit" value="Login">
				</div>
					<!-- <a href="#">Forgot Password ?</a> -->
			 </form>
		</div>
	</div>
		<div class="footer-agileits">
			<p>&copy; RESTAURANT | LogIn Form. All Rights Reserved | Design by <a href="https://casperindia.com/" target="_blank"> CasperIndia</a></p>
		</div>
</body>
</html>