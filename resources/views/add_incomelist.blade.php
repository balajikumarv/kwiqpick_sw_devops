<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('data_tables/css/jquery.dataTables.css') }}" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="{{ asset('css/SidebarNav.min.css') }}" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('js/modernizr.custom.js') }}"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="{{ asset('js/Chart.js') }}"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="{{ asset('js/metisMenu.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>

<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
	.border_table{
		border: solid 1px;
		border-color: #716d6d;
		margin-top: 0px;
		padding: 9px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<div id="page-wrapper">
	<div class="main-page">
		<div class="forms">
			<div class="row">
				<div class="form-three widget-shadow">
					<p class="statusMsg"></p>
					<form class="form-horizontal" enctype="multipart/form-data" id="insert_form" method="post">
						<h4><a href="{{ url('admin_incoming') }}">All Income</a></h4>
						<br>
						@if(count($add_incominglists) > 0)
        					@foreach($add_incominglists->all() as $add_lists)
						<div class="form-group">
							<label for="Bill Number" class="col-sm-2 control-label">Bill Number</label>
							<div class="col-sm-4">
								<input type="text" name="billno" placeholder="Bill No" class="form-control" id="billno" value="{{ $add_lists->bill_id }}" readonly="">
								<input type="hidden" name="record_number"  id="record_number" value="{{ $add_lists->record_number }}" readonly="">
							</div>
							<label for="Supplier" class="col-sm-1 control-label">Supplier</label>
							<div class="col-sm-4">
								<input type="hidden" readonly="" name="supplier_id" id="supplier_id" value="{{ $add_lists->supplier_id }}" class="form-control">
								<input type="text" readonly="" class="form-control" value="{{ $add_lists->supplier->name }}">
							</div>
							<div class="col-sm-2">
							</div>
						</div>
							@endforeach
	      				@endif
						<h4>Add Incomes</h4>
						<br>
						<span id="result"></span>
						<table class="table table-bordered" id="income_table">
							<tr>
								<th width="20%">Category</th>
								<th width="20%">Ingredient</th>
								<th width="20%">Unit</th>
								<th width="10%">Quantity</th>
								<th width="10%">Rate</th>
								<th width="10%">Tax</th>
								<th width="10%">Amount</th>
								
								<th>
									<i class="fa fa-cogs" aria-hidden="true"></i>
								</th>
							</tr>
							<tr>
								<td>
									<select class="form-control category" name="category_id[]" id="category_1">
										<option value="0">Select Category</option>
										@if(count($ingredient_categories) > 0)
											@foreach($ingredient_categories->all() as $ingredient_category)
												<option value="{{ $ingredient_category->id }}">{{ $ingredient_category->category }}</option>
											@endforeach
										@else
											<option value="-"> -- No Data -- </option>
										@endif
									</select>
								</td>
								<td>
									<select name="ingredient_id[]" id="ingredient_1" class="form-control ingredient" required="">
								    	<option value="-"> -- No Data -- </option>
			                		</select>
								</td>
								<td>
									<select name="unit_id[]" id="unit_1" class="form-control" required="">
								    	<option value="-"> -- No Data -- </option>
			                		</select>
								</td>
								<td><input type="text" name="quantity[]" class="form-control income_quantity" id="income_quantity_1" placeholder="0" /></td>
								<td><input type="text" name="rate[]" class="form-control income_rate" id="rate_1" placeholder="0.00" /></td>
								<td><input type="text" name="tax[]" class="form-control income_tax" id="tax_1" placeholder="0%" /></td>
								<td><input type="text" name="amount[]" class="form-control income_amount" id="income_amount_1" placeholder="0.00" />
									<input type="hidden" name="row_count" id="row_count" value="1">
								</td>
								<!-- <td><input type="text" name="total[]" class="form-control income_total" /></td> -->
								
								<td><button type="button" name="add" class="btn btn-success btn-sm add">
										<span class="glyphicon glyphicon-plus" style="color: #fff;"></span>
									</button></td>
							</tr>
						</table>
						<br>
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label"></label>
							<div class="col-sm-8">
								<input type="submit" name="submit" class="btn btn-success submitBtn" value="SAVE"/>
								<a href="{{ url('admin_incoming') }}" class="btn btn-danger">GO BACK</a>
							</div>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@include('inc.footer')
</div>
<script>
$(document).ready(function(){
 $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
 
 $(document).on('click', '.add', function(){
  var html = '';
  var row_count = $('#row_count').val();
  var row_count_val = +row_count+ 1;
  /*alert(row_count_val);*/
  $("#row_count").val(row_count_val);
  html += '<tr>';
  html += '<td>'+
  			'<select class="form-control category" name="category_id[]" id="category_'+row_count_val+'">'+
  				'<option value="0"> Select Category </option>'+
				'@if(count($ingredient_categories) > 0)'+
					'@foreach($ingredient_categories->all() as $ingredient_category)'+
						'<option value="{{ $ingredient_category->id }}">{{ $ingredient_category->category }}</option>'+
					'@endforeach'+
				'@else'+
					'<option value="-"> -- No Data -- </option>'+
				'@endif'+
			'</select>'+
			'</td>';
  html += '<td>'+
  			'<select name="ingredient_id[]" id="ingredient_'+row_count_val+'" class="form-control ingredient" required="">'+
		    	'<option value="-"> -- No Data -- </option>'+
    		'</select>';
  html += '<td>'+
  			'<select name="unit_id[]" id="unit_'+row_count_val+'" class="form-control" required="">'+
		    	'<option value="-"> -- No Data -- </option>'+
    		'</select>';
  html += '<td><input type="text" name="quantity[]" id="quantity_'+row_count_val+'" class="form-control income_quality" placeholder="0" /></td>';
  html += '<td><input type="text" name="rate[]" id="rate_'+row_count_val+'" class="form-control income_rate" placeholder="0.00"/></td>';
  html += '<td><input type="text" name="tax[]" id="tax_'+row_count_val+'" class="form-control income_tax" placeholder="0%"/></td>';
  html += '<td><input type="text" name="amount[]" id="amount_'+row_count_val+'" class="form-control income_amount" placeholder="0.00"/></td>';
  html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
  $('#income_table').append(html);
 });
 
 $(document).on('click', '.remove', function(){
  $(this).closest('tr').remove();
 });

  /*dropdown*/
 $(document).on('change', '.category', function(){
 	var el = this;
	var id = this.id;
	var splitid = id.split("_");
	var tr_row = splitid[1];
	/*alert(tr_row);*/
    var categoryID = $(this).val();    
    if(categoryID){
    	$.ajax({
           type:"GET",
           url:"{{url('api/get-ingredient-list')}}?category_id="+categoryID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
            	$('#ingredient_'+tr_row).empty();
            	$('#ingredient_'+tr_row).append('<option value="0">Select </option>');
                /*$(this).closest('tr').children('td').find('#ingredient').empty();
                $(this).closest('tr').children('td').find('#ingredient').append('<option>Select</option>');*/
                $.each(res,function(key,value){
                	$("#ingredient_"+tr_row).append('<option value="'+value['id']+'">'+value['ingredient']+'</option>');
                });
           
            }else{
               $("#ingredient_"+tr_row).empty();
            }
           }
        });
    }else{
        $("#ingredient_"+tr_row).empty();
    }      
   });
	 $(document).on('change', '.ingredient', function(){
	 	var el = this;
		var id = this.id;
		var splitid = id.split("_");
		var tr_row = splitid[1];

	    var ingredientID = $(this).val();    
	    if(ingredientID){
	    	$.ajax({
	           type:"GET",
	           url:"{{url('api/get-unit-list')}}?ingredient_id="+ingredientID,
	           success:function(res){    
	            if(res){
	            	/*console.log(res);*/
	                $("#unit_"+tr_row).empty();
	                /*$("#unit").append('<option>Select</option>');*/
	                $.each(res,function(key,value){
	                	$("#unit_"+tr_row).append('<option value="'+value['id']+'">'+value['unit']+'</option>');
	                });
	           
	            }else{
	               $("#unit_"+tr_row).empty();
	            }
	           }
	        });
	    }else{
	        $("#unit_"+tr_row).empty();
	    }      
	   });
 
 $('#insert_form').on('submit', function(event){
  event.preventDefault();	
  $.ajax({
    url:"{{ url('create_incominglist') }}", 
    method:"POST",
    data:$(this).serialize(),
    dataType:'json',
    beforeSend:function(){
    	$('#save').attr('disabled', 'disabled');
    },
    success:function(data)
    {
     if(data.error)
     {
      var error_html= '';
      for(var count = 0; count < data.error.length; count++){
      	error_html += '<p>'+data.error[count]+'</p>';
      }
      $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
     }
     else{
     	alert(data.success);
     	$('#insert_form').trigger("reset");
    	/*console.log(data.success);
    	$('result').html('<div class="alert alert-success">'+data.success+'</div>');*/
     }
     $('#save').attr('disabled', false);
    }
   });
  
 });
 
});
</script>
<!-- side nav js -->
	<script src="{{ asset('js/SidebarNav.min.js') }}" type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="{{ asset('js/classie.js') }}"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
	<script src="{{ asset('js/scripts.js') }}"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="{{ asset('js/bootstrap.js') }}"> </script>
	<!-- Data Tables -->
	<script src="{{ asset('data_tables/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('data_tables/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/jszip.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/pdfmake.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/vfs_fonts.js') }}"></script>
	<script src="{{ asset('data_tables/js/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/buttons.print.min.js') }}"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Incoming Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Incoming Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Incoming Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>

