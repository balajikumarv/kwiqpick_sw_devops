<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>
<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>Menu's Details<label  class="pull-right" data-toggle="modal" data-target="#add_menu">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Menu Type</th>
							<th>Branch</th>
							<th>Category</th>
							<th>Item</th>
							<th>Created</th>
							<th>Status</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($menus) > 0)
							@foreach($menus->all() as $menu)
						<tr>
							<th></th>
							<th scope="row">{{ $menu->menu_type }}</th>
							<td>{{ $menu->branch->branch_name }}</td>
							<td>{{ $menu->category->category }}</td>
							<td>{{ $menu->name }}</td>
							<td>{{ $menu->created_at->format('d/m/Y') }}</td>
							<td>
								@if ($menu->menu_status == 1)
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								@else
									<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								@endif
							</td>
							<td>
							<label data-toggle="modal" data-target="#view_menu{{ $menu->id }}"><span data-toggle="tooltip" title="View" class="cursor_point"><i class="fa fa-folder-open-o" aria-hidden="true"></i></span></label> | 
								<label data-toggle="modal" data-target="#edit_menu{{ $menu->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
								<label class="deleteData cursor_point" data-id="{{ $menu->id }}" id="{{ $menu->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>
							<!-- Start View Menu  -->
							  <div class="modal fade" id="view_menu{{ $menu->id }}" role="dialog">
							    <div class="modal-dialog modal-lg">
							    <!-- Modal content-->
							      <div class="modal-content">
							      	<form>
							        <div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4 class="modal-title">View Menu</h4>
							        </div>
							        <div class="modal-body">
							        	<div class="row">
							        		<h4>Menu Details</h4>
							        		<br>
							        		<div class="col-md-2">
												<label> Menu Type </label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_type }}</label>
											</div>
											<div class="col-md-2">
												<label> Branch</label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->branch_id }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Category </label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->category_id }}</label>
											</div>
											<div class="col-md-2">
												<label> Description</label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_desc }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Item </label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->name }}</label>
											</div>
											<div class="col-md-2">
												<label> Cuisine</label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_cuisine }}</label>
											</div>
							        	</div>
							        	<hr>
							        	<div class="row">
							        		<h4>Price & Tax Details</h4>
							        		<br>
							        		<div class="col-md-2">
												<label> Price </label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->price }}</label>
											</div>
											<div class="col-md-2">
												<label> Incl. Or Excl</label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_incl_excl }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Tax Slab</label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_tax }}</label>
											</div>
											<div class="col-md-2">
												<label> Addons </label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_addons }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Spicy Level</label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_item_spicy_level }}</label>
											</div>
											<div class="col-md-2">
												<label> Item Description </label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_item_desc }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Kitchen</label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->kitchen_id }}</label>
											</div>
											<div class="col-md-2">
												<label> Veg / Nonveg </label>
											</div>
											<div class="col-md-4">
												<label> {{ $menu->menu_item_veg_nonveg }}</label>
											</div>
							        	</div>
									</div>
									<div class="modal-footer">
							          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							        </div>
							        </form>
							      </div>
							    </div>
							  </div>
							<!-- // End View Menu -->
							<!-- Edit menu  -->
						  <div class="modal fade" id="edit_menu{{ $menu->id }}" role="dialog">
						    <div class="modal-dialog modal-lg">
						    <!-- Modal content-->
						      <div class="modal-content">
						      	<form  class="update_form" id="updateForm{{ $menu->id }}" data-toogle="validator">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Edit Menu</h4>
						        </div>
						        <div class="modal-body">
									<div class="row">
						        		<div class="col-md-4">
											<div class="form-group">
											    <label for="Name">Menu Type</label>
											    <select class="form-control" name="edit_menu_type" id="edit_menu_type">
											    	<option hidden="">--- Select ---</option>
											    	<option value="Food" {{$menu->menu_type == "Food"  ? 'selected' : ''}}>Food</option>
											    	<option value="Other" {{$menu->menu_type == "Other"  ? 'selected' : ''}}>Other</option>
											    </select>
											    <input type="hidden" class="form-control" name="menu_id" id="menu_id" value="{{ $menu->id }}">
											</div>			
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Name">Branches</label>
											    <select class="form-control" name="edit_menu_branch" id="edit_menu_branch">
											    	@if(count($branches) > 0)
														@foreach($branches->all() as $branch)
															<option value="{{ $branch->id }}" {{$menu->branch_id == "$branch->id"  ? 'selected' : ''}}>{{ $branch->branch_name }}</option>
														@endforeach
													@else
														<option value="-"> -- No Data -- </option>
													@endif
											    </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Number">Category</label>
											    <select class="form-control" name="edit_menu_category" id="edit_menu_category">
											    	<option hidden="">--- Select Category ---</option>
											    	@if(count($categories) > 0)
							        					@foreach($categories->all() as $category)
												    		<option value="{{ $category->id }}" {{$menu->category_id == "$category->id"  ? 'selected' : ''}}>{{ $category->category }}</option>
												    	@endforeach
								      				@else
								      				<option value="-"> -- No Data -- </option>
								      				@endif
											    </select>
											</div>
										</div>
						        	</div>
						        	<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Name">Kitchen</label>
											    <select class="form-control" name="edit_menu_kitchen" id="edit_menu_kitchen">
											    	<option hidden="" value="-">--- Select Kitchen ---</option>
											    	@if(count($kitchens) > 0)
							        					@foreach($kitchens->all() as $kitchen)
												    		<option value="{{ $kitchen->id }}" {{$menu->kitchen_id == "$kitchen->id"  ? 'selected' : ''}}>{{ $kitchen->name }}</option>
												    	@endforeach
								      				@else
								      				<option value="-"> -- No Data -- </option>
								      				@endif
											    </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Number">Item Spicy Levels</label>
											    <select class="form-control" name="edit_menu_item_spicy_level" id="edit_menu_item_spicy_level">
											    	<option hidden="">--- Select ---</option>
											    	<option value="Low" {{$menu->menu_item_spicy_level == "Low"  ? 'selected' : ''}}>Low</option>
											    	<option value="Medium" {{$menu->menu_item_spicy_level == "Medium"  ? 'selected' : ''}}>Medium</option>
											    	<option value="High" {{$menu->menu_item_spicy_level == "High"  ? 'selected' : ''}}>High</option>
											    </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Manager">Item</label>
											    <input type="text" class="form-control" id="edit_menu_item" name="edit_menu_item" placeholder="Enter Item" value="{{ $menu->name }}">
											</div>
										</div>
						        	</div>
						        	<div class="row">
						        		
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Manager">Cuisine</label>
											    <input type="text" class="form-control" id="edit_menu_cuisine" name="edit_menu_cuisine" placeholder="Enter Cuisine" value="{{ $menu->menu_cuisine }}">
											    <select class="form-control" name="edit_menu_cuisine" id="edit_menu_cuisine">
											    	<option hidden="" value="-">--- Select Cuisine ---</option>
											    	@if(count($cuisines) > 0)
							        					@foreach($cuisines->all() as $cuisine)
												    		<option value="{{ $cuisine->id }}" {{$menu->menu_cuisine == "$cuisine->id"  ? 'selected' : ''}}>{{ $cuisine->cuisine }}</option>
												    	@endforeach
								      				@else
								      				<option value="-"> -- No Data -- </option>
								      				@endif
											    </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Manager">Price</label>
											    <input type="text" class="form-control" id="edit_menu_price" name="edit_menu_price" placeholder="Enter Price" value="{{ $menu->price }}">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Manager">CGST(%)</label>
											    <input type="text" class="form-control" name="cgst" placeholder="Enter CGST" value="{{ $menu->cgst }}">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Manager">SGST(%)</label>
											    <input type="text" class="form-control" name="sgst" placeholder="Enter SGST" value="{{ $menu->sgst }}">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Manager">Short Code</label>
											    <input type="text" class="form-control" name="short_code" placeholder="Enter short code" value="{{ $menu->short_code }}">
											</div>
										</div>
										<div class="col-md-4">
											<label for="Manager"></label>
											<div class="form-group">
												<label class="container_radio">Full Plate.
												  <input type="radio" name="edit_menu_plate" value="0" {{ ($menu->plate=="0")? "checked" : "" }}>
												  <span class="checkmark_radio"></span>
												</label>
												<label class="container_radio">Others.
												  <input type="radio" name="edit_menu_plate" value="1" {{ ($menu->plate=="1")? "checked" : "" }}>
												  <span class="checkmark_radio"></span>
												</label>
											</div>	
										</div>
						        	</div>
						        	<div class="row other_plates" style="display: none;">
						        		<div class="col-md-4">
											<div class="form-group">
													<label for="Manager">Half Plate</label>
													<input type="text" class="form-control" name="edit_half_plate" placeholder="Enter Half Amount" value="{{ $menu->half_plate }}">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
													<label for="Manager">Family Pack</label>
													<input type="text" class="form-control" name="edit_familypack_plate" placeholder="Enter Family Pack Amount" value="{{ $menu->familypack_plate }}">
											</div>
										</div>
						        	</div>
						        	<div class="row">
						        		<div class="col-md-4">
											<div class="form-group">
											    <label for="Number">Description</label>
											    <textarea class="form-control" rows="5" placeholder="Category Description" name="edit_menu_desc" id="edit_menu_desc">{{ $menu->menu_desc }}</textarea>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="Number"></label>
													<div style="height: 114px;">
														<div id="c1" style="overflow: auto; max-height: 100px;">
														<!-- addon list -->
														<?php $tax_selected = $menu->taxes()->pluck('taxes.id');?>
														@if(count($taxes) > 0)
															@foreach($taxes->all() as $tax)
																<label class="container_checkbox">{{ $tax->category }}
																<input type="checkbox" name="edit_menu_tax[]" value="{{ $tax->id }}" {{ $tax_selected->contains($tax->id) ? 'checked' : '' }}>
																<span class="checkmark_checkbox"></span>
																</label>
															@endforeach
														@else
																<span>No Data In Tax</span>
														@endif
														<!-- end -->
														</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="Number">Addons</label>
													<div style="height: 114px;">
														<div id="c1" style="overflow: auto; max-height: 100px;">
														<!-- addon list -->
														<?php $addon_selected = $menu->addons()->pluck('addons.id');?>
														@if(count($addons) > 0)
															@foreach($addons->all() as $addon)
																<label class="container_checkbox">{{ $addon->name }}
																<input type="checkbox" name="edit_menu_addons[]" value="{{ $addon->id }}" {{ $addon_selected->contains($addon->id) ? 'checked' : '' }}>
																<span class="checkmark_checkbox"></span>
																</label>
															@endforeach
														@else
																<span>No Data In Addons</span>
														@endif
														<!-- end -->
														</div>
												</div>
											</div>
										</div>
						        	</div>
						        	<div class="row">
						        	<div class="col-md-4">
										<div class="form-group">
											<label class="container_radio">Veg.
												<input type="radio" name="edit_menu_item_veg_nonveg" value="1" {{ ($menu->menu_item_veg_nonveg=="1")? "checked" : "" }}>
											<span class="checkmark_radio"></span>
											</label>
											<label class="container_radio">NonVeg.
												<input type="radio" name="edit_menu_item_veg_nonveg" value="0" {{ ($menu->menu_item_veg_nonveg=="0")? "checked" : "" }}>
											<span class="checkmark_radio"></span>
											</label>
										</div>	
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="container_radio">Incl.
											  <input type="radio" name="edit_menu_incl_excl" value="1" {{ ($menu->menu_incl_excl=="1")? "checked" : "" }}>
											  <span class="checkmark_radio"></span>
											</label>
											<label class="container_radio">Excl.
											  <input type="radio" name="edit_menu_incl_excl" value="0" {{ ($menu->menu_incl_excl=="0")? "checked" : "" }}>
											  <span class="checkmark_radio"></span>
											</label>
										</div>	
									</div>
									<div class="col-md-4">
										<div class="form-group">
												<label class="container_radio">Active
												  <input type="radio" name="edit_menu_status" value="1" {{ ($menu->menu_status=="1")? "checked" : "" }}>
												  <span class="checkmark_radio"></span>
												</label>
												<label class="container_radio">Inactive
												  <input type="radio" value="0" name="edit_menu_status" {{ ($menu->menu_status=="0")? "checked" : "" }}>
												  <span class="checkmark_radio"></span>
												</label>
										</div>	
									</div>
						        </div>
								</div>
						        <div class="modal-footer">
						          <input type="hidden" value="{{$menu->id}}" class="menu_id"/>
								  <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						        </div>
						        </form>
						      </div>
						    </div>
						  </div>
						<!-- // End Menu -->
							@endforeach
	      				@endif
					</tbody>
				</table>
			<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- Add menu-->
  <div class="modal fade" id="add_menu" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
      	<form method="post" id="insert_form" enctype="multipart/form-data" data-toogle="validator">
          @csrf {{ method_field('POST') }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Menu</h4>
        </div>
        <div class="modal-body">
			<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Name">Menu Type</label>
					    <select class="form-control" name="menu_type" id="menu_type" required="">
					    	<option hidden="">--- Select Menu Type ---</option>
					    	<option>Food</option>
					    	<option>Other</option>
					    </select>
					</div>			
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Name">Branches</label>
					    <select class="form-control" name="menu_branch" id="menu_branch" required="">
					    	<option hidden="">--- Select Branch ---</option>
					    	@if(count($branches) > 0)
	        					@foreach($branches->all() as $branch)
						    		<option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
						    	@endforeach
		      				@else
		      				<option value="-"> -- No Data -- </option>
		      				@endif
					    </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Category</label>
					    <select class="form-control" name="menu_category" id="category" required="">
					    	<option hidden="">--- First Select Branch ---</option>
					    </select>
					</div>
				</div>
        	</div>
        	<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Name">Kitchen</label>
					    <select class="form-control" name="menu_kitchen" id="menu_kitchen" required="">
					    	<option hidden="" value="-">--- Select Kitchen ---</option>
					    	<!-- @if(count($kitchens) > 0)
	        					@foreach($kitchens->all() as $kitchen)
						    		<option value="{{ $kitchen->id }}">{{ $kitchen->name }}</option>
						    	@endforeach
		      				@else
		      				<option value="-"> -- No Data -- </option>
		      				@endif -->
					    </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Item Spicy Levels</label>
					    <select class="form-control" name="menu_item_spicy_level" id="menu_item_spicy_level" required="">
					    	<option hidden="">--- Select Item Spicy Levels ---</option>
					    	<option>Low</option>
					    	<option>Medium</option>
					    	<option>High</option>
					    </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Item</label>
					    <input type="text" class="form-control" id="menu_item" name="menu_item" placeholder="Enter Item" required="">
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Cuisine</label>
					    <select class="form-control" name="menu_cuisine" id="menu_cuisine" required="">
					    	<option hidden="">--- Select Cuisine ---</option>
					    	@if(count($cuisines) > 0)
	        					@foreach($cuisines->all() as $cuisine)
						    		<option value="{{ $cuisine->id }}">{{ $cuisine->cuisine }}</option>
						    	@endforeach
		      				@else
		      				<option value="-"> -- No Data -- </option>
		      				@endif
					    </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Price</label>
					    <input type="text" class="form-control" id="menu_price" name="menu_price" placeholder="Enter Price" required="">
					</div>					
				</div>
				<div class="col-md-4">
					<div class="form-group">
							<label for="Manager">CGST(%)</label>
							<input type="text" class="form-control" name="cgst" placeholder="Enter CGST" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
							<label for="Manager">SGST(%)</label>
							<input type="text" class="form-control" name="sgst" placeholder="Enter SGST" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
							<label for="Manager">Short Code</label>
							<input type="text" class="form-control" name="short_code" placeholder="Enter short code" required>
					</div>
				</div>
				<div class="col-md-4">
					<label for="Manager"></label>
					<div class="form-group">
						<label class="container_radio">Full Plate
						  <input type="radio" name="menu_plate" value="0" checked="">
						  <span class="checkmark_radio"></span>
						</label>
						<label class="container_radio">Others
						  <input type="radio" name="menu_plate" value="1">
						  <span class="checkmark_radio"></span>
						</label>
					</div>	
				</div>
        	</div>
        	<div class="row other_plates" style="display: none;">
        		<div class="col-md-4">
					<div class="form-group">
							<label for="Manager">Half Plate</label>
							<input type="text" class="form-control" name="half_plate" placeholder="Enter Half Amount">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
							<label for="Manager">Family Pack</label>
							<input type="text" class="form-control" name="familypack_plate" placeholder="Enter Family Pack Amount">
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Description</label>
					    <textarea class="form-control" rows="5" placeholder="Category Description" name="menu_desc" id="menu_desc" required=""></textarea>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <!-- <label for="Number">Item Description</label>
					    <textarea class="form-control" rows="5" placeholder="Item Description" name="menu_item_desc" id="menu_item_desc" required=""></textarea> -->
					    <label for="Number">Taxes</label>
						<div style="height: 114px;">
						  <div id="c1" style="overflow: auto; max-height: 100px;">
						   <!-- addon list -->
						   @if(count($taxes) > 0)
	        					@foreach($taxes->all() as $tax)
						    		<label class="container_checkbox">{{ $tax->category }} -  {{ $tax->percentage }} %
									  <input type="checkbox" name="menu_tax[]" value="{{ $tax->id }}">
									  <span class="checkmark_checkbox"></span>
									</label>
						    	@endforeach
		      				@else
		      				<span>No Data In Tax</span>
		      				@endif
							<!-- end -->
						  </div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="Number">Addons</label>
						<div style="height: 114px;">
						  <div id="c1" style="overflow: auto; max-height: 100px;">
						   <!-- addon list -->
						   @if(count($addons) > 0)
	        					@foreach($addons->all() as $addon)
						    		<label class="container_checkbox">{{ $addon->name }}
									  <input type="checkbox" name="menu_addons[]" value="{{ $addon->id }}">
									  <span class="checkmark_checkbox"></span>
									</label>
						    	@endforeach
		      				@else
		      				<span>No Data In Addons</span>
		      				@endif
							<!-- end -->
						  </div>
						</div>
					</div>
					
				</div>
        	</div>
        	<div class="row">
        	<div class="col-md-4">
				<div class="form-group">
					<label class="container_radio">Veg.
						<input type="radio" name="menu_item_veg_nonveg" value="1" checked="">
					<span class="checkmark_radio"></span>
					</label>
					<label class="container_radio">NonVeg.
						<input type="radio" name="menu_item_veg_nonveg" value="0">
					<span class="checkmark_radio"></span>
					</label>
				</div>	
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="container_radio">Incl.
					  <input type="radio" name="menu_incl_excl" value="1" checked="">
					  <span class="checkmark_radio"></span>
					</label>
					<label class="container_radio">Excl.
					  <input type="radio" name="menu_incl_excl" value="0">
					  <span class="checkmark_radio"></span>
					</label>
				</div>	
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label class="container_radio">Active
					  <input type="radio" name="menu_status" value="1" checked="">
					  <span class="checkmark_radio"></span>
					</label>
					<label class="container_radio">Inactive
					  <input type="radio" name="menu_status" value="0">
					  <span class="checkmark_radio"></span>
					</label>
				</div>	
			</div>
        </div>
		</div>
        <div class="modal-footer">
          <input type="submit" name="submit" class="btn btn-primary submitBtn" id="insertbutton" value="Save">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- // End menu -->
<!-- plate hide & show -->
<script type="text/javascript">
	$(document).ready(function() {
	    $("input[name$='menu_plate']").click(function() {
	        var plate = $(this).val();

	        if (plate == 1) {
	        	$(".other_plates").show();
	        }else{
	        	$(".other_plates").hide();
	        }
	       
	    });
	    $("input[name$='edit_menu_plate']").click(function() {
	        var plate = $(this).val();

	        if (plate == 1) {
	        	$(".other_plates").show();
	        }else{
	        	$(".other_plates").hide();
	        }
	       
	    });
	});
</script>
<!-- //plate hide & show -->
<!-- main content End-->
@include('inc.footer')
</div>
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
	var h1=$('#c1').height();
	var h2 = 150-h1;
	$('#c2').height(h2);

    $("#insert_form").on('submit', function(e){
      e.preventDefault();
      	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
            type: 'POST',
            url: "{{ url('create_menu') }}",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              /*$('#add_menu').modal('hide');
              $('#insert_form')[0].reset();
              swal("Good job!", "New Kitchen Created Successfully!", "success");
              window.location.href= 'admin_menu';*/
               $('#insert_form')[0].reset();
            },
            error : function(data){
              $('#add_menu').modal('hide');
              swal({
                title: 'Oops...',
                text: data.message,
                type: 'error',
                timer: '1500' 
              })
            }
        });
        return false;
    });

    //Edit
    $(".updateBtn").on('click', function(e){
      var id = $(this).parent().find('.menu_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "menu/edit",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_menu'+id).modal('hide');
              $('.update_form')[0].reset();
              swal("Good job!", "Menu Details Updated Successfully!", "success");
              window.location.href= 'admin_menu';
            },
            error : function(response){
              $('#edit_menu'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });

    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "menu/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});   

	$('#menu_branch').change(function(){
    var branchID = $(this).val();    
	    if(branchID){
	    	$.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    });
	        $.ajax({
	           type:"GET",
	           url:"{{url('api/get-category-list')}}?branch_id="+branchID,
	           success:function(res){    
	            if(res){
	            	console.log(res);
	                $("#category").empty();
	                $("#menu_kitchen").empty();
	                //categories
	                $("#category").append('<option value="">Select</option>');
	                $("#menu_kitchen").append('<option value="">Select</option>');
	                $.each(res.categories,function(key,value){
	                	$("#category").append('<option value="'+value['id']+'">'+value['category']+'</option>');
	                });
	                //Kitchens menu_kitchen
	                
	                $.each(res.kitchens,function(key,value){
	                	$("#menu_kitchen").append('<option value="'+value['id']+'">'+value['name']+'</option>');
	                });
	           
	            }else{
	               $("#category").empty();
	               $("#menu_kitchen").empty();
	            }
	           }
	        });
	    }else{
	        $("#category").empty();
	        $("#menu_kitchen").empty();
	    }      
    });

    $('#edit_menu_branch').change(function(){
    var branchID = $(this).val();    
    if(branchID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-category-list')}}?branch_id="+branchID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
                $("#edit_menu_category").empty();
                $("#edit_menu_kitchen").empty();
                //categories
                $("#edit_menu_category").append('<option value="">Select</option>');
                $.each(res.categories,function(key,value){
                	$("#edit_menu_category").append('<option value="'+value['id']+'">'+value['category']+'</option>');
                });
                //kitchens
                $("#edit_menu_kitchen").append('<option value="">Select</option>');
                $.each(res.kitchens,function(key,value){
	                	$("#edit_menu_kitchen").append('<option value="'+value['id']+'">'+value['name']+'</option>');
	                });
           
            }else{
               $("#edit_menu_category").empty();
               $("#edit_menu_kitchen").empty();
            }
           }
        });
    }else{
        $("#edit_menu_category").empty();
        $("#edit_menu_kitchen").empty();
    }      
   });

});


</script>
<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Menu Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Menu Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Menu Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>
