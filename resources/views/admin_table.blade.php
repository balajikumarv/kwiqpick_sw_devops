<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>
<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
.modal-content{
	border-radius: 0px;
}
.heading{
	margin-bottom: 2px;
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>Table's Details<label  class="pull-right" data-toggle="modal" data-target="#add_table">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Name</th>
							<th>Capacity</th>
							<th>Zone</th>
							<th>Created</th>
							<th>Status</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($tables) > 0)
        					@foreach($tables->all() as $table)
						<tr>
							<th></th>
							<th scope="row">{{ $table->name }}</th>
							<td>{{ $table->table_capacity }}</td>
							<td>{{ $table->zone->name }}</td>
							<td>{{ $table->created_at->format('d/m/Y') }}</td>
							<td>
								@if ($table->status == 1)
									<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								@else
									<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								@endif
							</td>
							<td>
							<label data-toggle="modal" data-target="#edit_table{{ $table->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
							<label class="deleteData cursor_point" data-id="{{ $table->id }}" id="{{ $table->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>

						<!-- Edit table  -->
						  <div class="modal fade" id="edit_table{{ $table->id }}" role="dialog">
						    <div class="modal-dialog modal-lg">
						    <!-- Modal content-->
						      <div class="modal-content">
						      	<form  class="update_form" id="updateForm{{ $table->id }}" data-toogle="validator">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Edit {{ $table->name }}</h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row">
						        		<div class="col-md-4">
						        			<div class="form-group">
												<label for="ID">Table Name</label>
												<input type="text" class="form-control" placeholder="Enter Table Name" id="edit_table_name" name="edit_table_name" value="{{ $table->name }}">
												<input type="hidden" class="form-control" name="table_id" id="table_id" value="{{ $table->id }}">
											</div> 	
						        		</div>
						        		<div class="col-md-4">
						        			<div class="form-group">
												<label for="ID">Table Capacity</label>
												<input type="text" class="form-control" placeholder="Enter Capacity" value="{{ $table->table_capacity }}" id="edit_table_capacity" name="edit_table_capacity">
											</div>
						        		</div>
						        		<div class="col-md-4">
						        			<div class="form-group">
											    <label for="Number">Branch</label>
											    <select class="form-control" name="edit_table_branch" id="edit_table_branch">
											    	@if(count($branches) > 0)
														@foreach($branches->all() as $branch)
															<option value="{{ $branch->id }}" {{$table->branch_id == "$branch->id"  ? 'selected' : ''}}>{{ $branch->branch_name }}</option>
														@endforeach
													@else
														<option value="-"> -- No Data -- </option>
													@endif
												</select>
											</div>
						        		</div>
						        	</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Number">Zone</label>
											    <select class="form-control" name="edit_table_zone" id="edit_table_zone">
											    	@if(count($zones) > 0)
														@foreach($zones->all() as $zone)
															<option value="{{ $zone->id }}" {{$table->zone_id == "$zone->id"  ? 'selected' : ''}}>{{ $zone->name }}</option>
														@endforeach
													@else
														<option value="-"> -- No Data -- </option>
													@endif
												</select>
											</div>	
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Number">Vendor</label>
											    <select class="form-control" name="edit_table_vendar" onchange="edit_change_vendar();" id="edit_table_vendar">
											    	<option value="0" hidden="">-- Select Vendor --</option>
											    	<option value="1" {{ ($table->table_vendor == "1")? 'selected' : "" }}>Branch</option>
											    	<option value="2" {{ ($table->table_vendor == "2")? 'selected' : "" }}>Online</option>
												</select>
											</div>
										</div>
										<div class="col-md-4" style="display: none;" id="show_edit_vendar_percentage">
											<div class="form-group">
											    <label for="Number">Percentage (%)</label>
											    <input type="number" class="form-control" step="0.1" min="0" max="100" placeholder="Enter Percentage (%)" id="edit_vendar_percentage" name="edit_vendar_percentage" value="{{ $table->vendar_percentage }}" required="">
											</div>	
										</div>
									</div>			
									<div class="row">
										<div class="form-group">
											<label class="container_radio">Active
											  <input type="radio" name="edit_table_status" value="1" {{ ($table->status == "1")? "checked" : "" }}>
											  <span class="checkmark_radio"></span>
											</label>
											<label class="container_radio">Inactive
											  <input type="radio" value="0" name="edit_table_status" {{ ($table->status == "0")? "checked" : "" }}>
											  <span class="checkmark_radio"></span>
											</label>
										</div>
									</div>
									</div>
						        <div class="modal-footer">
						         	<input type="hidden" value="{{$table->id}}" class="table_id"/>
								    <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						        </div>
						        </form>
						      </div>
						    </div>
						  </div>
						<!-- // End table -->
							@endforeach
	      				@endif
					</tbody>
				</table>
			<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- Add table  -->
<div class="modal fade" id="add_table" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
      	<form method="post" id="insert_form" enctype="multipart/form-data" data-toogle="validator">
          @csrf {{ method_field('POST') }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Table</h4>
        </div>
        <div class="modal-body">
        	<div class="row"> 
        		<div class="col-md-4">
        			<div class="form-group">
						<label for="ID">Table Name</label>
						<input type="text" class="form-control" placeholder="Enter Table Name" id="table_name" name="table_name" required="">
					</div>	
        		</div>
        		<div class="col-md-4">
        			<div class="form-group">
						<label for="ID">Table Capacity</label>
						<input type="text" class="form-control" placeholder="Enter Capacity" id="table_capacity" name="table_capacity" required="">
					</div>	
        		</div>
        		<div class="col-md-4">
        			<div class="form-group">
					    <label for="Number">Branch</label>
					    <select class="form-control" name="table_branch" id="table_branch" required="">
					    	<option hidden="">--- Select Branch ---</option>
					    	@if(count($branches) > 0)
	        					@foreach($branches->all() as $branch)
						    		<option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
						    	@endforeach
		      				@else
			      			<option value="-"> -- No Data -- </option>
			      			@endif
						</select>
					</div>	
        		</div>
        	</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Zone</label>
					    <select class="form-control" name="table_zone" id="table_zone" required="">
					    	<option hidden="">--- First Select Branch ---</option>
					    </select>
					</div>	
				</div>
				<div class="col-md-4">
        			<div class="form-group">
					    <label for="Number">Vendor </label>
					    <select class="form-control" name="table_vendar" id="table_vendar" onchange="change_vendar();" required="">
					    	<option hidden="" value="">--- Select Vendor ---</option>
					    	<option value="1">Branch Vendor </option>
					    	<option value="2">Online Vendor </option>
					    </select>
					</div>	
        		</div>
				<div class="col-md-4" style="display: none;" id="show_vendar_percentage">
					<div class="form-group">
					    <label for="Number">Percentage (%)</label>
					    <input type="number" class="form-control" step="0.1" min="0" max="100" placeholder="Enter Percentage (%)" id="vendar_percentage" name="vendar_percentage">
					</div>	
				</div>
					
			</div>  			
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="container_radio">Active
						  <input type="radio" name="table_status" value="1" checked="">
						  <span class="checkmark_radio"></span>
						</label>
						<label class="container_radio">Inactive
						  <input type="radio" name="table_status" value="0">
						  <span class="checkmark_radio"></span>
						</label>
					</div>		
				</div>
			</div>
			</div>
        <div class="modal-footer">
          <input type="submit" name="submit" class="btn btn-primary submitBtn" id="insertbutton" value="Save">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- // End table -->

<!-- main content End-->
@include('inc.footer')
</div>
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
    $("#insert_form").on('submit', function(e){
      e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "{{ url('create_table') }}",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              $('#add_table').modal('hide');
              $('#insert_form')[0].reset();
              swal("Good job!", "New Table Details Created Successfully!", "success");
              window.location.href= 'admin_table';
			},
            error : function(data){
              $('#add_table').modal('hide');
              swal({
                title: 'Oops...',
                text: data.message,
                type: 'error',
                timer: '1500' 
              })
            }
        });
        return false;
    });

    //Edit
    $(".updateBtn").on('click', function(e){
      var id = $(this).parent().find('.table_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "table/edit",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_table'+id).modal('hide');
              $('.update_form')[0].reset();
              window.location.href= 'admin_table';
              swal("Good job!", "Table Details Updated Successfully!", "success");
            },
            error : function(response){
              $('#edit_table'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });

    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "table/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	}); 

	$('#table_branch').change(function(){
    var branchID = $(this).val();  
    /*alert(branchID);  */
	    if(branchID){
	    	$.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    });
	        $.ajax({
	           type:"GET",
	           url:"{{url('api/get-zone-list')}}?branch_id="+branchID,
	           success:function(res){    
	            if(res){
	            	/*console.log(res);*/
	                $("#table_zone").empty();
	                $("#table_zone").append('<option value="">Select</option>');
	                $.each(res,function(key,value){
	                	$("#table_zone").append('<option value="'+value['id']+'">'+value['name']+'</option>');
	                });
	           
	            }else{
	               $("#table_zone").empty();
	            }
	           }
	        });
	    }else{
	        $("#table_zone").empty();
	    }      
    });  
    $('#edit_table_branch').change(function(){
    var branchID = $(this).val();    
    if(branchID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-zone-list')}}?branch_id="+branchID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
                $("#edit_table_zone").empty();
                $("#edit_table_zone").append('<option value="">Select</option>');
                $.each(res,function(key,value){
                	$("#edit_table_zone").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
           
            }else{
               $("#edit_table_zone").empty();
            }
           }
        });
    }else{
        $("#edit_table_zone").empty();
    }      
   });

});
//selecting vendar
function change_vendar(){
 var select_vendar = $('#table_vendar').val();

 if (select_vendar == '2') {
      $("#show_vendar_percentage").show();
 }
 else if(select_vendar == '1'){
      $("#show_vendar_percentage").hide();
 }
}
function edit_change_vendar(){
 var select_edit_vendar = $('#edit_table_vendar').val();

 if (select_edit_vendar == '2') {
      $("#show_edit_vendar_percentage").show();
 }
 else if(select_edit_vendar == '1'){
      $("#show_edit_vendar_percentage").hide();
 }
}
</script>

	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Table Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Table Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Table Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>