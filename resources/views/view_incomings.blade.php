<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
<link href="{{ asset('data_tables/css/jquery.dataTables.css') }}" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="{{ asset('css/SidebarNav.min.css') }}" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('js/modernizr.custom.js') }}"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="{{ asset('js/Chart.js') }}"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="{{ asset('js/metisMenu.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>

<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
	.border_table{
		border: solid 1px;
		border-color: #716d6d;
		margin-top: 0px;
		padding: 9px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4><a href="{{ url('admin_incoming') }}">All Incoming Data Details</a><a href='{{ url("/add_incomelist/{$id}") }}'><label  class="pull-right">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label></a></h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Bill ID</th>
							<th>Ingredient</th>
							<th>Category</th>
							<th>Unit</th>
							<th>Quantity</th>
							<th>Rate</th>
							<th>Amount</th>
							<th>Tax</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($view_incoms) > 0)
        					@foreach($view_incoms->all() as $view_income)
						<tr>
							<th></th>
							<th scope="row">
								{{ $view_income->bill_id }}
							</th>
							<th>{{ $view_income->ingredient->ingredient }}</th>
							<th>{{ $view_income->category->category }}</th>
							<th>{{ $view_income->unit->unit }}</th>
							<th>{{ $view_income->quantity }}</th>
							<th>{{ $view_income->rate }}</th>
							<th>{{ $view_income->amount }}</th>
							<th>{{ $view_income->tax }}</th>
							<td> 
								<label data-toggle="modal" data-target="#view_income{{ $view_income->id }}"><span data-toggle="tooltip" title="View" class="cursor_point"><i class="fa fa-folder-open-o" aria-hidden="true"></i></span></label> | 
								<label data-toggle="modal" data-target="#edit_income{{ $view_income->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
								<label class="deleteData cursor_point" data-id="{{ $view_income->id }}" id="{{ $view_income->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>

							<!-- Start View Menu  -->
							  <div class="modal fade" id="view_income{{ $view_income->id }}" role="dialog">
							    <div class="modal-dialog modal-lg">
							    <!-- Modal content-->
							      <div class="modal-content">
							      	<form>
							        <div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4 class="modal-title">View Incomes</h4>
							        </div>
							        <div class="modal-body">
							        	<div class="row">
							        		<h4>Item Details</h4>
							        		<br>
							        		<div class="col-md-2">
												<label> Record Number :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->record_number }}</label>
											</div>
											<div class="col-md-2">
												<label> Bill ID :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->bill_id }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Supplier :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->supplier->name }}</label>
											</div>
											<div class="col-md-2">
												<label> Ingredient :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->ingredient->ingredient }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Category :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->category->category }}</label>
											</div>
											<div class="col-md-2">
												<label> Quantity :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->quantity }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Unit :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->unit->unit }}</label>
											</div>
							        	</div>
							        	<hr>
							        	<div class="row">
							        		<h4>Price & Tax Details</h4>
							        		<br>
							        		<div class="col-md-2">
												<label> Rate :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->rate }}</label>
											</div>
											<div class="col-md-2">
												<label> Amount :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->amount }}</label>
											</div>
							        	</div>
							        	<div class="row">
							        		<div class="col-md-2">
												<label> Total :</label>
											</div>
											<div class="col-md-4">
												<label> Something</label>
											</div>
											<div class="col-md-2">
												<label> Tax :</label>
											</div>
											<div class="col-md-4">
												<label> {{ $view_income->tax }}</label>
											</div>
							        	</div>
									</div>
									<div class="modal-footer">
							          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							        </div>
							        </form>
							      </div>
							    </div>
							  </div>
							<!-- // End View Menu -->
							<!-- Edit menu  -->
						  <div class="modal fade" id="edit_income{{ $view_income->id }}" role="dialog">
						    <div class="modal-dialog modal-lg">
						    <!-- Modal content-->
						      <div class="modal-content">
						      	<form  class="update_form" id="updateForm{{ $view_income->id }}" data-toogle="validator">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Edit Incomes</h4>
						        </div>
						        <div class="modal-body">
									<div class="row">
						        		<div class="col-md-4">
											<div class="form-group">
											    <label for="Name">Record Number</label>
											    <input type="text" name="edit_record_no" id="edit_record_no" readonly="" class="form-control" value="{{ $view_income->record_number }}">
											    <input type="hidden" class="form-control" name="income_id" id="income_id" value="{{ $view_income->id }}">
											</div>			
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Name">Bill No</label>
											    <input type="text" name="edit_bill_no" id="edit_bill_no" class="form-control" readonly="" value="{{ $view_income->bill_id }}">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											    <label for="Number">Supplier</label>
											    <input type="text" readonly="" class="form-control" value="{{ $view_income->supplier->name }}">
											    <input type="hidden" name="edit_supplier_id" id="edit_supplier_id" class="form-control" readonly="" value="{{ $view_income->supplier_id }}">
											</div>
										</div>
						        	</div>
						        	<div class="row">
						        		<div class="col-md-3">
											<div class="form-group">
											    <label for="Name">Category</label>
											    <select class="form-control" name="edit_category_id" id="edit_category_id">
											    	<option value="0"> Select Category</option>
											    	@if(count($ingredient_categories) > 0)
														@foreach($ingredient_categories->all() as $ingredient_category)
															<option value="{{ $ingredient_category->id }}" {{$view_income->category_id == "$ingredient_category->id"  ? 'selected' : ''}}>{{ $ingredient_category->category }}</option>
														@endforeach
													@else
														<option value="-"> -- No Data -- </option>
													@endif
											    </select>
											</div>
										</div>
						        		<div class="col-md-3">
											<div class="form-group">
											    <label for="Name">Ingredient</label>
											    <select class="form-control" name="edit_ingredient" id="edit_ingredient">
											    	<option value="0">Select Ingredient</option>
											    	@if(count($ingredients) > 0)
							        					@foreach($ingredients->all() as $ingredient)
												    		<option value="{{ $ingredient->id }}" {{$view_income->ingredient_id == "$ingredient->id"  ? 'selected' : ''}}>{{ $ingredient->ingredient }}</option>
												    	@endforeach
								      				@else
								      				<option value="-"> -- No Data -- </option>
								      				@endif
											    </select>
											</div>			
										</div>
										<div class="col-md-3">
											<div class="form-group">
											    <label for="Name">Unit</label>
											   <select class="form-control" name="edit_unit" id="edit_unit">
											    	<option value="0">Select Unit</option>
											    	@if(count($units) > 0)
							        					@foreach($units->all() as $unit)
												    		<option value="{{ $unit->id }}" {{$view_income->unit_id == "$unit->id"  ? 'selected' : ''}}>{{ $unit->unit }}</option>
												    	@endforeach
								      				@else
								      				<option value="-"> -- No Data -- </option>
								      				@endif
											    </select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
											    <label for="Number">Quantity</label>
											    <input type="text" name="edit_quantity" id="edit_quantity" class="form-control" required="" value="{{ $view_income->quantity }}">
											</div>
										</div>
						        	</div>
						        	<div class="row">
						        		<div class="col-md-3">
											<div class="form-group">
											    <label for="Name">Rate</label>
											    <input type="text" name="edit_rate" id="edit_rate" required="" class="form-control" value="{{ $view_income->rate }}">
											</div>			
										</div>
										<div class="col-md-3">
											<div class="form-group">
											    <label for="Name">Tax</label>
											    <input type="text" name="edit_tax" id="edit_tax" class="form-control" required="" value="{{ $view_income->tax }}">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
											    <label for="Number">Amount</label>
											    <input type="text" name="edit_amount" id="edit_amount" class="form-control" required="" value="{{ $view_income->amount }}">
											</div>
										</div>
						        	</div>
								</div>
						        <div class="modal-footer">
						          <input type="hidden" value="{{$view_income->id}}" class="income_id"/>
								  <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						        </div>
						        </form>
						      </div>
						    </div>
						  </div>
						<!-- // End Menu -->
							@endforeach
	      				@endif
					</tbody>
				</table>
					<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
@include('inc.footer')
</div>
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
  	// Select Ingredient
  	$('#edit_category_id').change(function(){
    var categoryID = $(this).val();    
    if(categoryID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-ingredient-list')}}?category_id="+categoryID,
           success:function(res){    
            if(res){
                $("#edit_ingredient").empty();
                //categories
                $("#edit_ingredient").append('<option value="">Select</option>');
                $.each(res,function(key,value){
                	$("#edit_ingredient").append('<option value="'+value['id']+'">'+value['ingredient']+'</option>');
                });
                
            }else{
               $("#edit_ingredient").empty();
               $("#edit_unit").empty();     
            }
           }
        });
    }else{
        $("#edit_ingredient").empty();
        $("#edit_unit").empty();
    }      
   });
  	/****************************************************************************/
  	// Select Unit
  	$('#edit_ingredient').change(function(){
    var IngredientID = $(this).val();    
    if(IngredientID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-unit-list')}}?ingredient_id="+IngredientID,
           success:function(res){    
            if(res){
                $("#edit_unit").empty();
                //categories
                $("#edit_unit").append('<option value="">Select</option>');
                $.each(res,function(key,value){
                	$("#edit_unit").append('<option value="'+value['id']+'">'+value['unit']+'</option>');
                });
                
            }else{
               $("#edit_unit").empty();     
            }
           }
        });
    }else{
        $("#edit_unit").empty();
    }      
   });
  	/***********************************************************************************/
  	// Update Data
    $(".updateBtn").on('click', function(e){
      var id = $(this).parent().find('.income_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "{{ url('incoming/edit') }}",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_income'+id).modal('hide');
              $('.update_form')[0].reset();
              swal("Good job!", "New Income Updated Successfully!", "success");
              location.reload();
            },
            error : function(response){
              $('#edit_income'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });


    //Delete
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: `{{ url('/income/delete/${id}')}}`,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});   


});
</script>
<!-- side nav js -->
	<script src="{{ asset('js/SidebarNav.min.js') }}" type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="{{ asset('js/classie.js') }}"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
	<script src="{{ asset('js/scripts.js') }}"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="{{ asset('js/bootstrap.js') }}"> </script>
	<!-- Data Tables -->
	<script src="{{ asset('data_tables/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('data_tables/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/jszip.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/pdfmake.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/vfs_fonts.js') }}"></script>
	<script src="{{ asset('data_tables/js/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('data_tables/js/buttons.print.min.js') }}"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Incoming Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Incoming Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Incoming Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>

