<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="{{ asset('css/SidebarNav.min.css') }}" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('js/modernizr.custom.js') }}"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="{{ asset('js/Chart.js') }}"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="{{ asset('js/metisMenu.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<!--//Metis Menu -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
</style>
<style>
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}
.form-horizontal .control-label{
	text-align: left;
}
</style>

</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				@if(auth()->user()->role =='Admin')
				<h3>{{ $view_branch[0]->branch_name}} Branch Details</h3>
				<br>
					   <div class="col_3">
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-dollar icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>{{ $Bill_total }}</strong></h5>
			                      <span>Total Bill</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>{{ $zones }}</strong></h5>
			                      <span>Total Zones</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>{{ $tables }}</strong></h5>
			                      <span>Total Tables</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>{{ $users }}</strong></h5>
			                      <span>Total Users</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="col-md-3 widget">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>{{ $kitchen_order }}</strong></h5>
			                      <span>Total KOT</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="clearfix"> </div>
			        	<!-- <br>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-dollar icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>18</strong></h5>
			                      <span>Branches</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>15</strong></h5>
			                      <span>Online Admin</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$1012</strong></h5>
			                      <span>Admin</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$450</strong></h5>
			                      <span>Admin</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="col-md-3 widget">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>1450</strong></h5>
			                      <span>Total Admin</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="clearfix"> </div>
			        	<br>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-dollar icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>20</strong></h5>
			                      <span>Branches</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>30</strong></h5>
			                      <span>Online Admin</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$1012</strong></h5>
			                      <span>Admin</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$450</strong></h5>
			                      <span>Admin</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="col-md-3 widget">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>1450</strong></h5>
			                      <span>Total Admin</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="clearfix"> </div>
			        	<br>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-dollar icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>30</strong></h5>
			                      <span>Branches</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>50</strong></h5>
			                      <span>Online Admin</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$1012</strong></h5>
			                      <span>Admin</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$450</strong></h5>
			                      <span>Admin</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="col-md-3 widget">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>1450</strong></h5>
			                      <span>Total Admin</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="clearfix"> </div>
			        	<br> -->
			        	
					</div>
					 @elseif(auth()->user()->role =='Manager')
					   <div class="col_3">
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-dollar icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$452

			                      		
			                      </strong></h5>
			                      <span>Total Manager</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$1019</strong></h5>
			                      <span>Online Manager</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$1012</strong></h5>
			                      <span>Manager</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$450</strong></h5>
			                      <span>Manager</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="col-md-3 widget">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>1450</strong></h5>
			                      <span>Total Manager</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="clearfix"> </div>
					</div>
					 
					 @elseif(auth()->user()->role =='Staff')
					   <div class="col_3">
				        	<div class="col-md-3 widget widget1">
				        		<div class="r3_counter_box">
				                    <i class="pull-left fa fa-dollar icon-rounded"></i>
				                    <div class="stats">
				                      <h5><strong>$452

				                      		
				                      </strong></h5>
				                      <span>Total Staff</span>
				                    </div>
				                </div>
				        	</div>
				        	<div class="col-md-3 widget widget1">
				        		<div class="r3_counter_box">
				                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
				                    <div class="stats">
				                      <h5><strong>$1019</strong></h5>
				                      <span>Online Staff</span>
				                    </div>
				                </div>
				        	</div>
				        	<div class="col-md-3 widget widget1">
				        		<div class="r3_counter_box">
				                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
				                    <div class="stats">
				                      <h5><strong>$1012</strong></h5>
				                      <span>Staff</span>
				                    </div>
				                </div>
				        	</div>
				        	<div class="col-md-3 widget widget1">
				        		<div class="r3_counter_box">
				                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
				                    <div class="stats">
				                      <h5><strong>$450</strong></h5>
				                      <span>Staff</span>
				                    </div>
				                </div>
				        	 </div>
				        	<div class="col-md-3 widget">
				        		<div class="r3_counter_box">
				                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
				                    <div class="stats">
				                      <h5><strong>1450</strong></h5>
				                      <span>Total Staff</span>
				                    </div>
				                </div>
				        	 </div>
				        	<div class="clearfix"> </div>
						</div>
					 
					 @elseif(auth()->user()->role =='Chef')
					   <div class="col_3">
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-dollar icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$452

			                      		
			                      </strong></h5>
			                      <span>Total Chef</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$1019</strong></h5>
			                      <span>Online Chef</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$1012</strong></h5>
			                      <span>Chef</span>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-3 widget widget1">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>$450</strong></h5>
			                      <span>Chef</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="col-md-3 widget">
			        		<div class="r3_counter_box">
			                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
			                    <div class="stats">
			                      <h5><strong>1450</strong></h5>
			                      <span>Total Chef</span>
			                    </div>
			                </div>
			        	 </div>
			        	<div class="clearfix"> </div>
					</div>
				@endif


				
			</div>
		</div>
<!--footer-->
		@include('inc.footer')
    <!--//footer-->
	</div>
		
	<!-- new added graphs chart js-->
	
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="{{ asset('js/classie.js') }}"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
		
	<!--scrolling js-->
	<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
	<script src="{{ asset('js/scripts.js') }}"></script>
	<!--//scrolling js-->
	
	<!-- side nav js -->
	<script src="{{ asset('js/SidebarNav.min.js') }}" type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="{{ asset('js/bootstrap.js') }}"> </script>
	<!-- //Bootstrap Core JavaScript -->
	
</body>
</html>