<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>
<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
.modal-content{
	border-radius: 0px;
}
.heading{
	margin-bottom: 2px;
}
</style>
<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 300px;
  margin: auto;
  text-align: center;
  font-family: arial;
  position: relative;
}

.title {
  color: grey;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}


button:hover, a:hover {
  opacity: 0.7;
}
.kot_clr{
	text-align: center;
    font-weight: bold;
    padding: 40px;
    margin: 0px;
    background-color: gold;
}
.kot_list{
	background-color: #fff;
}
.kot_list h4{
	padding: 10px;
}
.kot_pm{
	padding: 10px;
	margin-right: 1.5%;
	position: relative;
}
/* common */
.ribbon {
  width: 130px;
  height: 150px;
  overflow: hidden;
  position: absolute;
}
.ribbon::before,
.ribbon::after {
  position: absolute;
  z-index: -1;
  content: '';
  display: block;
  border: 5px solid #2980b9;
}
.ribbon span {
  position: absolute;
  display: block;
  width: 225px;
  padding: 3px 0;
  background-color: #3498db;
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
  color: #fff;
  /*font: 700 18px/1 'Lato', sans-serif;*/
  text-shadow: 0 1px 1px rgba(0,0,0,.2);
  text-transform: uppercase;
  text-align: center;
}

/* top left*/
.ribbon-top-left {
  top: -2px;
  left: -2px;
}
.ribbon-top-left::before,
.ribbon-top-left::after {
  border-top-color: transparent;
  border-left-color: transparent;
}
.ribbon-top-left::before {
  top: 0;
  right: 0;
}
.ribbon-top-left::after {
  bottom: 0;
  left: 0;
}
.ribbon-top-left span {
  right: -10px;
  top: 30px;
  transform: rotate(-45deg);
}
#x {
    position: relative;
    float: right;
    color: #222d32;
    cursor: pointer;
    top: -7px;
    right: -7px;
    font-size: 20px;
    padding: 4px;
    display: none;
}
.card:hover #x {
  display: block;
}
.span_sz{
	font-size: 14px;
}
.hr_list{
	margin-top: 1px;
	margin-bottom: 5px;
}
.li_type{
	text-align: left;
}
.li_type_span{
	padding-right: 15px;
}
.mb{
	margin-bottom: 0px;
}
</style>
</head> 
<body class="cbp-spmenu-push">
<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="col_3">
			@if(count($kitchen_orders) > 0)
				@foreach($kitchen_orders->all() as $kitchen_order)
			<div class="col-md-3 widget widget1 kot_pm">
			   	<div class="card">
					<i id = "x" class="fa fa-times-circle" aria-hidden="true"></i>
			        <div class="kot_clr">
					  	<h4>KOT - {{ $kitchen_order->id }}</h4>
					 </div>
				  	<div class="kot_list">
				  		<?php
				  			$datetime = $kitchen_order->created_at;
							$date = date('Y-m-d', strtotime($datetime));
							$time = date('H:i:s', strtotime($datetime));
				  		?>
				  		<h4>{{ $kitchen_order->table->name }} <span class="pull-right span_sz"><?php echo $time;?></span></h4>
				  		<!-- <hr class="hr_list"/>
					  	<div class="row mb">
					  		<ol>
						  		<li class="li_type" style="">Chicken <span class="pull-right li_type_span">5</span></li>	
						  		<li class="li_type" style="">Kabab <span class="pull-right li_type_span">2</span></li>	
						  		<li class="li_type" style="">Tandoori <span class="pull-right li_type_span">10</span></li>	
						  		<li class="li_type" style="">Tandoori <span class="pull-right li_type_span">10</span></li>	
						  		<li class="li_type" style="">Tandoori <span class="pull-right li_type_span">10</span></li>	
						  		<li class="li_type" style="">Tandoori <span class="pull-right li_type_span">10</span></li>		
						  	</ol>	
					  	</div> -->
				  	</div>
					<p><button>Accept</button></p>
				</div>
			</div>
				@endforeach
			@else
				<p> -- No KOT -- </p>
			@endif
        	
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- main content End-->
@include('inc.footer')
</div>
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>

</body>
</html>