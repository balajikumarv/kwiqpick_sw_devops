<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>

<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
	.border_table{
		border: solid 1px;
		border-color: #716d6d;
		margin-top: 0px;
		padding: 9px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>Item Management Details<label  class="pull-right" data-toggle="modal" data-target="#add_imanagement">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Name</th>
							<th>Unit</th>
							<th>Quantity</th>
							<th>Category</th>
							<th>Created On</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($itemmanagements) > 0)
        					@foreach($itemmanagements->all() as $itemmanagement)
						<tr>
							<th></th>
							<th scope="row">
								{{ $itemmanagement->name }}
							</th>
							<td>{{ $itemmanagement->unit }}</td>
							<td>{{ $itemmanagement->quantity }}</td>
							<td>{{ $itemmanagement->category }}</td>
							<td>{{ $itemmanagement->created_at }}</td>
							<td>
								<label data-toggle="modal" data-target="#edit_imanagement{{ $itemmanagement->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
								<label class="deleteData cursor_point" data-id="{{ $itemmanagement->id }}" id="{{ $itemmanagement->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>
						<!-- Start Edit itemmanagement  -->
								  <div class="modal fade" id="edit_imanagement{{ $itemmanagement->id }}" role="dialog">
								    <div class="modal-dialog modal-lg">
								    <!-- Modal content-->
								      <div class="modal-content">
								      	<form  class="update_form" id="updateForm{{ $itemmanagement->id }}" data-toogle="validator">
								        <div class="modal-header">
								          <button type="button" class="close" data-dismiss="modal">&times;</button>
								          <h4 class="modal-title">Edit {{ $itemmanagement->name }} Details</h4>
								          <div class="loading-overlay"><div class="overlay-content">Loading.....</div></div>
								        </div>
								        <div class="modal-body">
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
														<label for="Name">Ingredient Name</label>
														<input type="text" class="form-control" placeholder="Enter Name" id="edit_imanagement_name" name="edit_imanagement_name" value="{{ $itemmanagement->name }}">
														<input type="hidden" name="itemmanagement_id" id="itemmanagement_id" value="{{ $itemmanagement->id }}">
													</div>  			
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Unit">Ingredient Unit</label>
													    <input type="number" class="form-control" placeholder="Enter Unit" id="edit_imanagement_unit" name="edit_imanagement_unit" value="{{ $itemmanagement->unit }}">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Quantity">Ingredient unit Qty</label>
													    <input type="number" class="form-control" placeholder="Enter Quantity" id="edit_imanagement_quantity" name="edit_imanagement_quantity" value="{{ $itemmanagement->quantity }}">
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Address">Category</label>
													    <select class="form-control" id="edit_imanagement_category" name="edit_imanagement_category">
													    	<option value="0" {{$itemmanagement->category == "0"  ? 'selected' : ''}}>Select Category</option>
													    	<option value="1" {{$itemmanagement->category == "1"  ? 'selected' : ''}}>Dry Items</option>
													    	<option value="2" {{$itemmanagement->category == "2"  ? 'selected' : ''}}>Wet Items</option>
													    	<option value="3" {{$itemmanagement->category == "3"  ? 'selected' : ''}}>Liquid Items</option>
													    	<option value="4" {{$itemmanagement->category == "4"  ? 'selected' : ''}}>Perishable Items</option>
													    	<option value="5" {{$itemmanagement->category == "5"  ? 'selected' : ''}}>Non Perishable Items</option>
													    	
													    </select>
													</div>
												</div>
											</div>
								        </div>
								        <div class="modal-footer">
								        	<input type="hidden" value="{{$itemmanagement->id}}" class="itemmanagement_id"/>
                          				  <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								        </div>

										</div>
								        </form>
								      </div>
								    </div>
								  </div>
								<!-- // End Edit itemmanagement -->
							@endforeach
	      				@endif
					</tbody>
				</table>
					<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- popups -->
	<!-- Add -->
<div class="modal fade" id="add_imanagement" role="dialog">
	<div class="modal-dialog modal-lg">
	    <!-- Modal content-->
	    <div class="modal-content">
	      	<form method="post" id="insert_form" enctype="multipart/form-data" data-toogle="validator">
	          @csrf {{ method_field('POST') }} 
	        	<div class="modal-header">
	          		<button type="button" class="close" data-dismiss="modal">&times;</button>
	          		<h4 class="modal-title">Add Item Management</h4>
	        	</div>
		        <div class="modal-body">
		        	<div class="row">
		        		<div class="col-md-4">
							<div class="form-group">
								<label for="NAME">Ingredient Name</label>
								<input type="text" class="form-control" placeholder="Enter Name" id="imanagement_name" name="imanagement_name" required="">
							</div>  			
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label for="NUMBER">Ingredient Unit</label>
							    <input type="number" class="form-control" placeholder="Enter Unit" id="imanagement_unit" name="imanagement_unit" required="">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
							    <label for="ALTER.NUMBER">Ingredient unit Qty</label>
							    <input type="number" class="form-control" placeholder="Enter Ingredient unit Qty" id="imanagement_quantity" name="imanagement_quantity" required="">
							</div>
						</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-md-4">
							<div class="form-group">
							    <select class="form-control" id="imanagement_category" name="imanagement_category">
							    	<option value="0">Select Category</option>
							    	<option value="1">Dry Items</option>
							    	<option value="2">Wet Items</option>
							    	<option value="3">Liquid Items</option>
							    	<option value="4">Perishable Items</option>
							    	<option value="5">Non Perishable Items</option>
							    	
							    </select>
							</div>
						</div>
					</div>
		        </div>
		        <div class="modal-footer">
		          <input type="submit" name="submit" class="btn btn-primary submitBtn" id="insertbutton" value="Save">
		          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		        </div>
	    	</form>
		</div>
	</div>
</div>
	<!-- //Add -->
<!-- //End Popups -->

@include('inc.footer')
</div>
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
    $("#insert_form").on('submit', function(e){
      e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "{{ url('create_imanagement') }}",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              $('#add_imanagement').modal('hide');
              $('#insert_form')[0].reset();
              window.location.href= 'admin_item_management';
              swal("Good job!", "New Staff Details Created Successfully!", "success");

            },
            error : function(data){
              $('#add_imanagement').modal('hide');
              swal({
                title: 'Oops...',
                text: data.message,
                type: 'error',
                timer: '1500' 
              })
            }
        });
        return false;
    });

    //Edit
    $(".updateBtn").on('click', function(e){
      var id = $(this).parent().find('.itemmanagement_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "itemmanagement/edit",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_imanagement'+id).modal('hide');
              $('.update_form')[0].reset();
              window.location.href= 'admin_item_management';
              swal("Good job!", "Supplier Details Updated Successfully!", "success");
            },
            error : function(response){
              $('#edit_imanagement'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });

    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "itemmanagement/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});   

});


</script>
<!-- password Validation -->
<script type="text/javascript">
	var password = document.getElementById("staff_password")
  , confirm_password = document.getElementById("c_staff_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
<!-- //end validation for password -->
<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Item Management Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Item Management Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Item Management Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>

