<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
<aside class="sidebar-left">
    <nav class="navbar navbar-inverse">
      <div class="navbar-header" style="height: 56px;">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <h1><a class="navbar-brand" href="{{ url('/') }}"><!-- <span class="fa fa-area-chart"></span> --> RESTAURANT<span class="dashboard_text">Design dashboard</span></a></h1>
      </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              	<li class="header">Branch</li>
	            <!-- <li class="treeview">
	            	<a href="{{ url('admin') }}">
	                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
	                </a>
	            </li> -->
				<li class="treeview">
                	<a href="#">
                		<i class="fa fa-university"></i>
                		<span>Branch Details</span>
                		<i class="fa fa-angle-left pull-right"></i>
                	</a>
	                <ul class="treeview-menu">
	                	@can('Manage branches')
	                	<li><a href="{{ url('admin_branches') }}"><i class="fa fa-angle-right"></i> Branches</a></li>
	                	@endcan
	                	@if(auth()->user()->hasAnyPermission(['Manage zones', 'Manage branch zones']))
	                  	<li><a href="{{ url('admin_zone') }}"><i class="fa fa-angle-right"></i> Zone</a></li>
	                  	@endif
	                  	@if(auth()->user()->hasAnyPermission(['Manage users', 'Manage branch users']))
	                  	<li><a href="{{ url('admin_staff') }}"><i class="fa fa-angle-right"></i> Staff</a></li>
	                  	
	                  	@endif
	                  	@if(auth()->user()->hasAnyPermission(['Manage kitchens', 'Manage branch kitchens']))
	                  	<li><a href="{{ url('admin_kitchen') }}"><i class="fa fa-angle-right"></i> Kitchen</a></li>
	                  	@endif
	                  	@if(auth()->user()->hasAnyPermission(['Manage tables', 'Manage branch tables']))
	                  	<li><a href="{{ url('admin_table') }}"><i class="fa fa-angle-right"></i> Tables</a></li>
	                  	<li><a href="{{ url('branch_incomings') }}"><i class="fa fa-angle-right"></i> Incomings</a></li>
	                  	@endif
	                </ul>
	            </li>

              	@if(auth()->user()->hasAnyPermission(['Manage items', 'Manage branch items', 'Manage cuisine', 'Manage branch cuisine', 'Manage addons', 'Manage branch addons', 'Manage category', 'Manage branch category']))
              	<li class="treeview">
                	<a href="#">
                		<i class="fa fa-book"></i>
                		<span>Menu's</span>
                		<i class="fa fa-angle-left pull-right"></i>
                	</a>
                	<ul class="treeview-menu">
						@if(auth()->user()->hasAnyPermission(['Manage category', 'Manage branch category']))
                  			<li><a href="{{ url('admin_category') }}"><i class="fa fa-angle-right"></i> Category</a></li>
						@endif
						@if(auth()->user()->hasAnyPermission(['Manage cuisine', 'Manage branch cuisine']))
                  			<li><a href="{{ url('admin_cuisine') }}"><i class="fa fa-angle-right"></i> Cuisine</a></li>
						@endif
						@if(auth()->user()->hasAnyPermission(['Manage items', 'Manage branch items']))
							<li><a href="{{ url('admin_menu') }}"><i class="fa fa-angle-right"></i> Items</a></li>
						@endif
						
						@if(auth()->user()->hasAnyPermission(['Manage addons', 'Manage branch addons']))
                  			<li><a href="{{ url('admin_addon') }}"><i class="fa fa-angle-right"></i> Add-ons</a></li>
						@endif
						
						<!-- <li><a href="{{ url('admin_menu_type') }}"><i class="fa fa-angle-right"></i> Menu Type</a></li> -->
                	</ul>
              	</li>
				@endif
				@if(auth()->user()->hasAnyPermission(['Manage tax', 'Manage billtitle']))							
              	<li class="treeview">
                	<a href="#">
                		<i class="fa fa-cogs"></i>
                		<span>Settings</span>
                		<i class="fa fa-angle-left pull-right"></i>
                	</a>
	                <ul class="treeview-menu">
	                	@can('Manage tax')
	                  <li><a href="{{ url('admin_tax') }}"><i class="fa fa-angle-right"></i> Tax</a></li>
	                  @endcan
	                  @can('Manage billtitle')
	                  <li><a href="{{ url('admin_billtitle') }}"><i class="fa fa-angle-right"></i> Title's</a></li>
	                  @endcan
	                </ul>
              	</li>
              	@endif
              	@if(auth()->user()->hasAnyPermission(['Manage billing', 'Manage billing']))
              	<li class="treeview">
                	<a href="{{ url('billing') }}">
	                	<i class="fa fa-laptop"></i>
	                 	<span>Billing</span>
                	</a>
              	</li>
				<li class="treeview">
                	<a href="{{ url('receipts') }}">
                		<i class="fa fa-laptop"></i>
                 		<span>Bill Receipts</span>
                	</a>
              	</li>
              	@endif
              	@if(auth()->user()->hasAnyPermission(['Manage kot', 'Manage branch kot']))
              	<li class="treeview">
                	<a href="{{ url('kot') }}">
                		<i class="fa fa-ticket"></i>
                 		<span>KOT</span>
                	</a>
              	</li>
              	@endif
              	<li class="header">Inventory</li>
              	<li class="treeview">
					<a href="#">
						<i class="fa fa-cogs"></i>
						<span>Inventory</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						@can('Manage branches')
						<li class="treeview">
							<a href="#">
								<i class="fa fa-cogs"></i>
								<span>Controls</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="{{ url('ingredient_category') }}"><i class="fa fa-angle-right"></i> Category</a></li>
								<li><a href="{{ url('ingredient_unit') }}"><i class="fa fa-angle-right"></i> Unit</a></li>
								<li><a href="{{ url('ingredient') }}"><i class="fa fa-angle-right"></i> Ingredient</a></li>
						  		<li><a href="{{ url('admin_recipe') }}"><i class="fa fa-angle-right"></i> Recipe Management</a></li>
						  	</ul>
						</li>
						<li><a href="{{ url('admin_supplier') }}"><i class="fa fa-angle-right"></i> Supplier</a></li>
				  		<li><a href="{{ url('admin_incoming') }}"><i class="fa fa-angle-right"></i> Incoming</a></li>
				  		<li><a href="{{ url('admin_outgoing') }}"><i class="fa fa-angle-right"></i> Outgoing</a></li>
				  		<li><a href="{{ url('stocks') }}"><i class="fa fa-angle-right"></i> Stocks</a></li>
				  		<li><a href="{{ url('admin_report') }}"><i class="fa fa-angle-right"></i> Report</a></li>
				  		@endcan
				  	</ul>
				</li>
			</ul>
        </div>
    </nav>
</aside>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--toggle button end-->
				<div class="profile_details_left"><!--notifications of menu start -->
					
					<div class="clearfix"> </div>
				</div>
				<!--notification menu end -->
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
				<!--search-box-->
				<div class="search-box">
				</div><!--//end-search-box-->
				
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<span class="prfil-img">
										<img src="images/staff_images/{!! auth()->user()->image !!}" alt="" class="staff_icon_header">
									</span> 
									<div class="user-name">
										<p>{!! auth()->user()->name !!}</p>
										<span>{!! auth()->user()->email !!}</span>
									</div>
									<i class="fa fa-angle-down lnr"></i>
									<i class="fa fa-angle-up lnr"></i>
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu">
								<li> <a href="{!! url('/logout') !!}"><i class="fa fa-sign-out"></i> Logout</a> </li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clearfix"> </div>				
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->