<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>

<!-- //SweetAlert -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
	.border_table{
		border: solid 1px;
		border-color: #716d6d;
		margin-top: 0px;
		padding: 9px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>User's Details<label  class="pull-right" data-toggle="modal" data-target="#add_staff">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th class="no-export">Image</th>
							<th>Name</th>
							<th>Role</th>
							<th>Branch</th>
							<th>Number</th>
							<th>ID Type</th>
							<th>Created On</th>
							<th class="no-export">Status</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($staffs) > 0)
        					@foreach($staffs->all() as $staff)
						<tr>
							<th></th>
							<th scope="row">
								@if ($staff->image != '')
		        					<img src="images/staff_images/{{ $staff->image }}" class="staff_img_size">
		        				@else
		        					<img src="images/demo_staff.png" class="staff_img_size">
		        				@endif
							</th>
							<td>{{ $staff->name }}</td>
							<td>{{ implode(', ', $staff->getRoleNames()->toArray()) }}</td>
							<td>{{ isset($staff->branch->branch_name) ? $staff->branch->branch_name : '-' }}</td>
							<td>{{ $staff->number }}</td>
							<td>{{ $staff->id_type }}</td>
							<td>{{ $staff->created_at }}</td>
							<td>
								@if($staff->status == 1)
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								@else
								<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								@endif
							</td>
							<td>
								<label data-toggle="modal" data-target="#view_staff{{ $staff->id }}"><span data-toggle="tooltip" title="View" class="cursor_point"><i class="fa fa-folder-open-o" aria-hidden="true"></i></span></label> | 
								<label data-toggle="modal" data-target="#edit_staff{{ $staff->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
								<label class="deleteData cursor_point" data-id="{{ $staff->id }}" id="{{ $staff->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>
						<!-- Start View Staff  -->
						  <div class="modal fade" id="view_staff{{ $staff->id }}" role="dialog">
						    <div class="modal-dialog modal-lg">
						    <!-- Modal content-->
						      <div class="modal-content">
						      	<form>
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">View {{ $staff->staff_name }} Profile</h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row">
						        		<div class="col-md-4">
											<center>
												@if ($staff->image != '')
						        					<img src="images/staff_images/{{ $staff->image }}" class="staff_img_size">
						        				@else
						        					<img src="images/demo_staff.png" class="staff_img_size">
						        				@endif
											</center>
										</div>
										<div class="col-md-6">
											<div class="col-md-6 heading">
												<label class="label label-success"> ID Type </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $staff->id_type }}</label>
											</div>
											<br>
											<div class="col-md-6 heading">
												<label class="label label-success"> ID Number </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $staff->id_number }}</label>
											</div>
											<br>
											<div class="col-md-6 heading">
												<label class="label label-success"> Branch </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $staff->branch->branch_name }}</label>
											</div>
											<br>
											<div class="col-md-6 heading">
												<label class="label label-success"> Role </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $staff->role }}</label>
											</div>
										</div>
						        	</div>
						        	<div class="row">
						        		<h4>Staff Details</h4>
						        		<br>
						        		<div class="col-md-2">
											<label> Name :</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->name }}</label>
										</div>
										<div class="col-md-2">
											<label> Number</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->number }}</label>
										</div>
						        	</div>
						        	<div class="row">
						        		<div class="col-md-2">
											<label> User Name :</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->username }}</label>
										</div>
										<div class="col-md-2">
											<label> Password</label>
										</div>
										<div class="col-md-4">
											<label style="color: red;"> Cant To See </label>
										</div>
						        	</div>
						        	<div class="row">
						        		<div class="col-md-2">
											<label> E-Mail :</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->email }}</label>
										</div>
										<div class="col-md-2">
											<label> Alternate Number</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->alternate_number }}</label>
										</div>
						        	</div>
						        	<div class="row">
						        		<h4>Bank Details</h4>
						        		<br>
						        		<div class="col-md-2">
											<label> A/c No :</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->bank_account_number }}</label>
										</div>
										<div class="col-md-2">
											<label> IFSC</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->bank_ifsc }}</label>
										</div>
						        	</div>
						        	<div class="row">
						        		<div class="col-md-2">
											<label> Bank :</label>
										</div>
										<div class="col-md-4">
											<label> {{ $staff->bank_name }}</label>
										</div>
						        	</div>
								</div>
						        <div class="modal-footer">
						         <!--  <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
						          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
						        </div>
						        </form>
						      </div>
						    </div>
						  </div>
						<!-- // End View staff -->
						<!-- Start Edit Staff  -->
								  <div class="modal fade" id="edit_staff{{ $staff->id }}" role="dialog">
								    <div class="modal-dialog modal-lg">
								    <!-- Modal content-->
								      <div class="modal-content">
								      	<form  class="update_form" id="updateForm{{ $staff->id }}" data-toogle="validator">
								        <div class="modal-header">
								          <button type="button" class="close" data-dismiss="modal">&times;</button>
								          <h4 class="modal-title">Edit {{ $staff->name }} Details</h4>
								          <div class="loading-overlay"><div class="overlay-content">Loading.....</div></div>
								        </div>
								        <div class="modal-body">
								        	<div class="row">
								        		<div class="col-md-4">
								        			<div class="form-group">
								        				@if ($staff->image != '')
								        				<img src="images/staff_images/{{ $staff->image }}" class="staff_img_size">
								        				@else
								        				<img src="images/demo_staff.png" class="staff_img_size">
								        				@endif
										        		<label>Choose Image</label>
										        		<input type="file" name="edit_staff_image">
								        			</div>
								        		</div>	
								        	</div>
								        	
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
														<label for="ID">Name</label>
														<input type="text" class="form-control" placeholder="Enter Staff Name" id="edit_staff_name" name="edit_staff_name" value="{{ $staff->name }}">
														<input type="hidden" class="form-control" name="staff_id" id="staff_id" value="{{ $staff->id }}">
													</div>  			
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Name">Role</label>
													    @php
													    	$roles = $staff->getRoleNames()->toArray();
													    	$role = count($roles) ? $roles[0] : '';
													    @endphp
													    <select class="form-control" name="edit_staff_role" id="edit_staff_role">
													    	<option hidden="">--- Select Role ---</option>
													    	<option value="Admin" {{$role == "Admin"  ? 'selected' : ''}}>Admin</option>
													    	<option value="Manager" {{$role == "Manager"  ? 'selected' : ''}}>Manager</option>
													    	<option value="Staff" {{$role == "Staff"  ? 'selected' : ''}}>Staff</option>
													    	<option value="Chef" {{$role == "Chef"  ? 'selected' : ''}}>Chef</option>
													    </select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">Branch</label>
													    <select class="form-control" name="edit_staff_branch" id="edit_staff_branch">
														@if(count($branches) > 0)
															@foreach($branches->all() as $branch)
																<option value="{{ $branch->id }}" {{$staff->branch_id == "$branch->id"  ? 'selected' : ''}}>{{ $branch->branch_name }}</option>
															@endforeach
														@else
															<option value="-"> -- No Data -- </option>
														@endif

													    </select>
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">User Name</label>
													    <input type="text" class="form-control" id="edit_staff_user_name" name="edit_staff_user_name" value="{{ $staff->username }}" placeholder="Enter User Name">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Password</label>
													    <input type="password" class="form-control" id="edit_staff_password" name="edit_staff_password" value="{{ $staff->password }}" placeholder="Enter Password">
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">E-Mail</label>
													    <input type="email" class="form-control" id="edit_staff_email" value="{{ $staff->email }}" name="edit_staff_email" placeholder="Enter E-Mail Address">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Mobile Number</label>
													    <input type="text" class="form-control" id="edit_staff_number" value="{{ $staff->number }}" name="edit_staff_number" placeholder="Enter Mobile Number">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Alternate Number</label>
													    <input type="text" class="form-control" id="edit_staff_alternate_number" value="{{ $staff->alternate_number }}" name="edit_staff_alternate_number" placeholder="Enter Alternate Number">
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">ID Type</label>
													    <select class="form-control" name="edit_staff_id_type" id="edit_staff_id_type">
													    	<option hidden="">--- Select ID Type---</option>
													    	<option value="Type One" {{$staff->id_type == "Type One"  ? 'selected' : ''}}>Type One</option>
													    	<option value="Type Two" {{$staff->id_type == "Type Two"  ? 'selected' : ''}}>Type Two</option>
													    </select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">ID Number</label>
													    <input type="text" class="form-control" id="edit_staff_id_number" name="edit_staff_id_number" value="{{ $staff->id_number }}" placeholder="Enter Mobile Number">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Address</label>
													    <textarea class="form-control" name="edit_staff_address" id="edit_staff_address" placeholder="Address">{{ $staff->address }}</textarea>
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<h4 class="modal-title">Bank Details</h4>
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">Bank Name</label>
													    <input type="text" class="form-control" id="edit_staff_bank_name" name="edit_staff_bank_name" placeholder="Enter Bank Name" value="{{ $staff->bank_name }}">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Account No</label>
													    <input type="text" class="form-control" id="edit_staff_bank_account_number" name="edit_staff_bank_account_number" placeholder="Enter Account Number" value="{{ $staff->bank_account_number }}">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">IFSC Code</label>
													    <input type="text" class="form-control" id="edit_staff_bank_ifsc" name="edit_staff_bank_ifsc" placeholder="Enter IFSC Code" value="{{ $staff->bank_ifsc }}">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label class="container_radio">Active
														  <input type="radio" name="edit_staff_status" value="1" {{ ($staff->status=="1")? "checked" : "" }}>
														  <span class="checkmark_radio"></span>
														</label>
														<label class="container_radio">Inactive
														  <input type="radio" name="edit_staff_status" value="0" {{ ($staff->status=="0")? "checked" : "" }}>
														  <span class="checkmark_radio"></span>
														</label>
													</div>
												</div>
												
								        	</div>
										</div>
								        <div class="modal-footer">
                          				<input type="hidden" value="{{$staff->id}}" class="staff_id"/>
								          <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								        </div>

										</div>
								        </form>
								      </div>
								    </div>
								  </div>
								<!-- // End Edit Staff -->
							@endforeach
	      				@endif
					</tbody>
				</table>
					<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- Add Staff  -->
  <div class="modal fade" id="add_staff" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
      	<form method="post" id="insert_form" enctype="multipart/form-data" data-toogle="validator">
          @csrf {{ method_field('POST') }} 
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Staff</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
						<label for="ID">Name</label>
						<input type="text" class="form-control" placeholder="Enter Staff Name" id="staff_name" name="staff_name" required="">
					</div>  			
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Name">Role</label>
					    <select class="form-control" name="staff_role" id="staff_role" required="">
					    	<option hidden="">--- Select Role ---</option>
					    	<option>Admin</option>
					    	<option>Manager</option>
					    	<option>Staff</option>
					    	<option>Chef</option>
					    </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Branch</label>
					    <select class="form-control" name="staff_branch" id="staff_branch" required="">
					    	<option hidden="">--- Select Branch ---</option>
					    	@if(count($branches) > 0)
	        					@foreach($branches->all() as $branch)
						    		<option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
						    	@endforeach
		      				@else
		      				<option value="-"> -- No Data -- </option>
		      				@endif
					    </select>
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">User Name</label>
					    <input type="text" class="form-control" id="staff_user_name" name="staff_user_name" placeholder="Enter User Name" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Password</label>
					    <input type="password" class="form-control" id="staff_password" name="staff_password" placeholder="Enter Password" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Confirm Password</label>
					    <input type="password" class="form-control" id="c_staff_password" name="c_pass" placeholder="Enter Confirm Password" required="">
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">E-Mail</label>
					    <input type="email" class="form-control" id="staff_email" name="staff_email" placeholder="Enter E-Mail Address" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Mobile Number</label>
					    <input type="text" class="form-control" id="staff_number" name="staff_number" placeholder="Enter Mobile Number" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Alternate Number</label>
					    <input type="text" class="form-control" id="staff_alternate_number" name="staff_alternate_number" placeholder="Enter Alternate Number" required="">
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">ID Type</label>
					    <select class="form-control" name="staff_id_type" id="staff_id_type" required="">
					    	<option hidden="">--- Select ID Type---</option>
					    	<option>Type One</option>
					    	<option>Type Two</option>
					    </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">ID Number</label>
					    <input type="text" class="form-control" id="staff_id_number" name="staff_id_number" placeholder="Enter Mobile Number" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Address</label>
					    <textarea class="form-control" name="staff_address" id="staff_address" placeholder="Address" required=""></textarea>
					</div>
				</div>
        	</div>
        	<div class="row">
        		<h4 class="modal-title">Bank Details</h4>
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Bank Name</label>
					    <input type="text" class="form-control" id="staff_bank_name" name="staff_bank_name" placeholder="Enter Bank Name" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Account No</label>
					    <input type="text" class="form-control" id="staff_bank_account_number" name="staff_bank_account_number" placeholder="Enter Account Number" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">IFSC Code</label>
					    <input type="text" class="form-control" id="staff_bank_ifsc" name="staff_bank_ifsc" placeholder="Enter IFSC Code" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
		        		<label>Staff Image</label>
		        		<input type="file" name="staff_image" id="staff_image" required="">
        			</div>	
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="container_radio">Active
						  <input type="radio" name="staff_status" value="1" checked="">
						  <span class="checkmark_radio"></span>
						</label>
						<label class="container_radio">Inactive
						  <input type="radio" name="staff_status" value="0">
						  <span class="checkmark_radio"></span>
						</label>
					</div>
				</div>
        	</div>
		</div>
        <div class="modal-footer">
          <input type="submit" name="submit" class="btn btn-primary submitBtn" id="insertbutton" value="Save">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- // End Staff -->

@include('inc.footer')
</div>
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
    $("#insert_form").on('submit', function(e){
      e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "{{ url('create_staff') }}",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              $('#add_staff').modal('hide');
              $('#insert_form')[0].reset();
              window.location.href= 'admin_staff';
              setTimeout(function () {
                        window.location.href= 'admin_staff'; // the redirect goes here

                    },1000);
              swal("Good job!", "New Staff Details Created Successfully!", "success");

            },
            error : function(data){
              $('#add_staff').modal('hide');
              swal({
                title: 'Oops...',
                text: data.message,
                type: 'error',
                timer: '1500' 
              })
            }
        });
        return false;
    });

    //Edit
    $(".updateBtn").on('click', function(e){
      var id = $(this).parent().find('.staff_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "staff/edit",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_staff'+id).modal('hide');
              $('.update_form')[0].reset();
              window.location.href= 'admin_staff';
              swal("Good job!", "Staff Details Updated Successfully!", "success");
            },
            error : function(response){
              $('#edit_staff'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });

    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "staff/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});   

});


</script>
<!-- password Validation -->
<script type="text/javascript">
	var password = document.getElementById("staff_password")
  , confirm_password = document.getElementById("c_staff_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
<!-- //end validation for password -->
<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Staff Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Staff Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Staff Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>

