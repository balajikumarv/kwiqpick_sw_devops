import axios from 'axios';
import { type } from 'os';

const getZones = async () => {
    return await axios.get('api/get-master').then(res => res.data)
}
const placeOrder = (data) => {
    return axios.post('api/place-order', data).then(res => res.data)
}
const getPendingOrders = async () => {
    return await axios.get('api/orders/pending').then(res => res.data)
}
const printBill = async (orderIds, billId) => {
    return await axios.post('api/kitchen-orders/print', { order: orderIds, bill: billId }).then(res => res.data)
}
const billPayment = async (billId, type) => {
    return await axios.post(`api/orders/${billId}/payment`, { payment_type: type }).then(res => res.data)
}

const checkKOTStatus = async (orderIds) => {
    return await axios.post('api/kitchen-orders/status', { kot_orders: orderIds }).then(res => res.data)
}

export default {
    getZones,
    placeOrder,
    getPendingOrders,
    printBill,
    billPayment,
    checkKOTStatus,
    baseUrl: window.axios.defaults.baseURL
}