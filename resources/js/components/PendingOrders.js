import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import API from '../services/ApiService'

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
export default class PendingOrders extends Component {
	constructor() {
		super()
		this.state = {
			pendingOrders: [],
			showMenuItem: [],
			loading: false
		}
	}
	handleToggleProductList(id) {
		const showMenuItem = this.state.showMenuItem;
		const index = showMenuItem.indexOf(id);
		if(index == -1) {
			showMenuItem.push(id)
		}else{
			showMenuItem.splice(index, 1)
		}
		this.setState({showMenuItem})
	}

	async componentDidMount () {
		this.setState({loading: true});
		const pendingOrders = await API.getPendingOrders();
		this.setState({pendingOrders, loading: false});
	}
	render () {
		return <div className="row">
			  	{this.state.pendingOrders.map(order => 
				<div className="col-sm-12 wthree-crd widgettable" key={order.id}>
					<div className="card">
						<div className="card-body">
							<div className="agileinfo-cdr">
                                <div className="card-header">
                                    <h3 style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <span>#{ order.invoice_no }</span>
                                    <span>{order.table.name}</span>
                                    <span>Sub total: {order.sub_total}</span>
                                    <span>CGST: {order.cgst}</span>
                                    <span>SGST: {order.sgst}</span>
                                    <span>Net total: {order.net_total}</span>
                                    <button type="button" onClick={() => this.handleToggleProductList(order.id)}>{this.state.showMenuItem.indexOf(order.id) >=0 ? 'Hide' : 'Show'} products</button>
                                    </h3>
                                </div>
                                {this.state.showMenuItem.indexOf(order.id) >=0 &&
		                            <div className="widget-body">
		                                <table className="table table-bordered">
		                                	<thead>
		                                		<tr>
		                                            <th>Name</th>
		                                            <th>Quantity</th>
		                                            <th>Amount</th>
		                                            <th>Total amount</th>
		                                        </tr>
		                                    </thead>
		                                    <tbody>
												{order.items.map(item => {
													return <tr key={item.id}>
														<td>{ item.name }</td>
														<td>{ item.quantity }</td>
                        								<td>{ item.price }</td>
                        								<td>{ item.total_amount }</td>
													</tr>
												})}
		                                    </tbody>
		                                </table>
		                            </div>
		                        }
                            </div>
                        </div>
                    </div>
                </div>
			    )}
		</div>
	}
}

if (document.getElementById('pending-orders')) {
    ReactDOM.render(<PendingOrders />, document.getElementById('pending-orders'));
}